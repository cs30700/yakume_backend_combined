//Thanks to http://stackoverflow.com/questions/1485708/how-do-i-do-a-http-get-in-java

import javax.net.ssl.HttpsURLConnection;
import javax.script.ScriptContext;
import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HTTPRequest {
    public static String request(String urlWithParameters, HTTPMethod method) {
        StringBuilder result = new StringBuilder();
        try {
            URL u = new URL(urlWithParameters);
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            String httpMethod = "GET";
            if (method == null) {
                httpMethod = "GET";
            } else if (method == HTTPMethod.POST) {
                httpMethod = "POST";
            } else if (method == HTTPMethod.GET) {
                httpMethod = "GET";
            }
            conn.setRequestMethod(httpMethod);
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
            return result.toString();
        } catch (Exception e) {
            return e.toString();
        }
    }
    public static String request(String urlWithParameters, HTTPMethod method, Map<String, String> headers) {
        StringBuilder result = new StringBuilder();
        try {
            URL u = new URL(urlWithParameters);
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            String httpMethod = "GET";
            if (method == null) {
                httpMethod = "GET";
            } else if (method == HTTPMethod.POST) {
                httpMethod = "POST";
            } else if (method == HTTPMethod.GET) {
                httpMethod = "GET";
            }
            conn.setRequestMethod(httpMethod);
            for (String s : headers.keySet()) {
                conn.setRequestProperty(s, headers.get(s));
            }
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
            return result.toString();
        } catch (Exception e) {
            return e.toString();
        }
    }

    public static String requestSSL(String url, HTTPMethod method, Map<String, String> params) {
        try {
            String query = "";
            if (params != null) {
                for (String key : params.keySet()) {
                    if (!query.equals("")) {
                        query += "&";
                    }
                    query += key + "=" + URLEncoder.encode(params.get(key), "UTF-8");
                }
            }

            URL myurl = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) myurl.openConnection();
            if (method == HTTPMethod.POST)
                con.setRequestMethod("POST");
            else
                con.setRequestMethod("GET");

            con.setRequestProperty("Content-length", String.valueOf(query.length()));
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setDoOutput(true);
            con.setDoInput(true);

            DataOutputStream output = new DataOutputStream(con.getOutputStream());
            output.writeBytes(query);
            output.close();
            Scanner s = new Scanner(con.getInputStream());
            StringBuffer sb = new StringBuffer();
            while (s.hasNextLine()) {
                sb.append(s.nextLine());
                sb.append('\n');
            }

            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "An error occured.";
        }
    }
}
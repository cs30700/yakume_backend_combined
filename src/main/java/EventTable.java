import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by yuxuan on 10/1/16.
 */

/**
 * DB Schema
 * CREATE TABLE events(
         id INTEGER PRIMARY KEY AUTOINCREMENT,
         timeposted LONG,
         timeofevent LONG,
         duration INTEGER,
         longitude DOUBLE,
         latitude DOUBLE,
         address VARCHAR(512),
         zipcode INTEGER,
         title VARCHAR(1024),
         description VARCHAR(2048),
         owner VARCHAR(256),
         price INT NULL,
         recur INT
   );
 *
 */

public class EventTable
{
    SQLDBConnection dbConn;

    public EventTable(SQLDBConnection dbConn) throws SQLException {
        this.dbConn = dbConn;
        dbConn.connect();

    }

    synchronized public Event findEventById(int id) {
        Event retval = null;
        String sqlStmt = String.format("SELECT * FROM events WHERE id=%d;", id);
        Statement stmt = null;
        ResultSet rset = null;
        try {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            if (rset.next()) {
                retval = new Event();
                retval.setId(rset.getInt("id"));
                retval.setTimePosted(rset.getLong("timeposted"));
                retval.setTimeOfEvent(rset.getLong("timeofevent"));
                retval.setSecondsDuration(rset.getInt("duration"));
                double longitude = rset.getDouble("longitude");
                double latitude = rset.getDouble("latitude");
                int zipcode = rset.getInt("zipcode");
                String address = rset.getString("address");
                Location l = new Location(zipcode, address, longitude, latitude);
                l.address = rset.getString("address");
                l.zipcode = rset.getInt("zipcode");
                retval.setLocation(l);
                retval.setPrice(rset.getInt("price"));
                retval.setRecurring(rset.getInt("recur"));
                retval.setTitle(rset.getString("title"));
                retval.setDescription(rset.getString("description"));
                retval.setOwnerEmail(rset.getString("owner"));
            }
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return null;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    YakumeServer.logger.print(e.toString());
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    synchronized public int putEvent(Event event) {
        if (event == null) {
            return -3;
        }
        long timePosted = event.getTimePosted();
        long timeOfEvent = event.getTimeOfEvent();
        int duration = event.getSecondsDuration();
        double longitude = 0, latitude = 0;
        String address = null;
        int zipcode = 0;
        if (event.getLocation() != null) {
            longitude = event.getLocation().longitude;
            latitude = event.getLocation().latitude;
            address = event.getLocation().address;
            zipcode = event.getLocation().zipcode;
        }

        String title = event.getTitle();
        String description = event.getDescription();
        String owner = event.getOwnerEmail();
        int price = event.getPrice();
        int recur = event.getRecurring();
        String sqlStmt =
                String.format("INSERT INTO events(timeposted, timeofevent, duration, longitude, latitude, address, zipcode, title, description, owner, price, recur) " +
                "VALUES (%d, %d, %d, %f, %f, \"%s\", %d, \"%s\", \"%s\", \"%s\", %d, %d);",
                timePosted, timeOfEvent, duration, longitude, latitude, address, zipcode, title, description, owner, price, recur);
        int check_val;
        try {
            check_val = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return -1; //Failed
        }
        if (check_val == 1) {
            return lastInsertId();
        } else {
            return -2;
        }
    }

    public Iterator<Event> iterator() {
        return getAllEvents().iterator();
    }

    public ArrayList<Event> getAllEvents() {
       return searchEventsBySQLStatement("SELECT * FROM events;");
    }

    synchronized public int deleteEventById(int id) {
        String sqlStmt = String.format("DELETE FROM events WHERE id=%d;", id);
        int check_val;
        try {
            check_val = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return 1; //Failed
        }
        return check_val == 1 ? 0 : 1;
    }

    public ArrayList<Event> searchEventByTime(long start, long end) {
        String sqlStmt = String.format("SELECT * FROM events WHERE timeofevent >= %d AND timeofevent < %d;", start, end);
        return searchEventsBySQLStatement(sqlStmt);
    }

    public ArrayList<Event> searchEventByOwner(String userEmail) {
        if (userEmail == null) {
            return new ArrayList<>();
        }
        String sqlStmt = String.format("SELECT * FROM events WHERE owner=\"%s\";", userEmail);
        return searchEventsBySQLStatement(sqlStmt);
    }

    public ArrayList<Event> searchEventByZipcode(int zipcode) {
        String sqlStmt = String.format("SELECT * FROM events WHERE zipcode=%d;", zipcode);
        return searchEventsBySQLStatement(sqlStmt);
    }

    public ArrayList<Event> searchEventByTitle(String title) {
        if (title == null) {
            return new ArrayList<>();
        }
        String sqlStmt = String.format("SELECT * FROM events WHERE title=\"%s\";", title);
        return searchEventsBySQLStatement(sqlStmt);
    }

    public ArrayList<Event> searchEventsBySQLStatement(String sqlStmt) {
        ArrayList<Event> retval = new ArrayList<>();
        Statement stmt = null;
        ResultSet rset = null;
        try {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            while (rset.next()) {
                Event toadd = new Event();
                toadd.setId(rset.getInt("id"));
                toadd.setTimePosted(rset.getLong("timeposted"));
                toadd.setTimeOfEvent(rset.getLong("timeofevent"));
                toadd.setSecondsDuration(rset.getInt("duration"));
                double longitude = rset.getDouble("longitude");
                double latitude = rset.getDouble("latitude");
                int zipcode = rset.getInt("zipcode");
                String address = rset.getString("address");
                Location l = new Location(zipcode, address, longitude, latitude);
                toadd.setLocation(l);
                toadd.setTitle(rset.getString("title"));
                toadd.setDescription(rset.getString("description"));
                toadd.setOwnerEmail(rset.getString("owner"));
                toadd.setPrice(rset.getInt("price"));
                toadd.setRecurring(rset.getInt("recur"));
                retval.add(toadd);
            }
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return null;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    YakumeServer.logger.print(e.toString());
                    e.printStackTrace();
                }
            }
        }

        return retval;
    }

    public ArrayList<Integer> filterEvent(int zipcode, long startTime, long endTime) {
        StringBuilder sb = new StringBuilder("SELECT * FROM events WHERE 1 ");
        if (zipcode != 0) {
            sb.append(String.format("AND zipcode=%d ", zipcode));
        }
        if (startTime >= 0) {
            sb.append(String.format("AND timeofevent >= %d ", startTime));
        }
        if (endTime >= 0) {
            sb.append(String.format("AND timeofevent <= %d", endTime));
        }
        sb.append(";");
        ArrayList<Event> ev = searchEventsBySQLStatement(sb.toString());
        ArrayList<Integer> ar = new ArrayList<>();
        for (int i = 0; i < ev.size(); i++) {
            ar.add(ev.get(i).getId());
        }
        return ar;
    }

    synchronized public int modifyEvent(int id, Event new_event) {
        long timePosted = new_event.getTimePosted();
        long timeOfEvent = new_event.getTimeOfEvent();
        int duration = new_event.getSecondsDuration();
        double longitude = 0, latitude = 0;
        String address = null;
        int zipcode = 0;
        if (new_event.getLocation() != null) {
            longitude = new_event.getLocation().longitude;
            latitude = new_event.getLocation().latitude;
            address = new_event.getLocation().address;
            zipcode = new_event.getLocation().zipcode;
        }
        String title = new_event.getTitle();
        String description = new_event.getDescription();
        int price = new_event.getPrice();
        int recur = new_event.getRecurring();
        String sqlStmt = String.format("UPDATE events SET timeposted=%d, timeofevent=%d, duration=%d, longitude=%f, latitude=%f, address=\"%s\", " +
                "zipcode=%d, title=\"%s\", description=\"%s\", price=%d, recur=%d WHERE id=%d;",
                timePosted, timeOfEvent, duration, longitude, latitude, address, zipcode, title, description, price, recur, id);
        int check_val;
        try {
            check_val = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return -1; //Failed
        }
        return check_val == 1 ? 0 : 1;
    }

    public int lastInsertId() {
        String lookupLast = "SELECT last_insert_rowid() AS id FROM events;";
        Statement stmt = null;
        ResultSet rset = null;
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(lookupLast);
            if (rset.next()) {
                return rset.getInt("id");
            }
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return -1;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    YakumeServer.logger.print(e.toString());
                    e.printStackTrace();
                }
            }
        }
        return -1;
    }
}

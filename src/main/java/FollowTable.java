import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * DB Schema
 * CREATE TABLE follow_list(
 *     follower VARCHAR(256),
 *     followee VARCHAR(256)
 * );
 */
public class FollowTable {
    SQLDBConnection dbConn;

    public FollowTable(SQLDBConnection dbConn) throws SQLException {
        this.dbConn = dbConn;
        dbConn.connect();
    }

    public int follow(String followerEmail, String followeeEmail) {
        if (hasFollowed(followerEmail, followeeEmail)) {
            return 0;
        }
        String sqlStmt = String.format("INSERT INTO follow_list VALUES(\"%s\", \"%s\");", followerEmail, followeeEmail);
        int check = 0;
        try {
            check = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check == 1 ? 0 : 1;
    }

    public int unfollow(String followerEmail, String followeeEmail) {
        String sqlStmt = String.format("DELETE FROM follow_list WHERE follower=\"%s\" AND followee=\"%s\";", followerEmail, followeeEmail);
        int check = 0;
        try {
            check = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check == 1 ? 0 : 1;
    }

    public boolean hasFollowed(String followerEmail, String followeeEmail) {
        String sqlStmt = String.format("SELECT 1 FROM follow_list WHERE follower=\"%s\" AND followee=\"%s\";", followerEmail, followeeEmail);
        int check = 0;
        Statement stmt = null;
        ResultSet rset;
        boolean retval = false;
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            if (rset.next()) {
                retval = true;
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public ArrayList<String> getFollowingList(String userEmail) {
        String sqlStmt = String.format("SELECT followee FROM follow_list WHERE follower=\"%s\";", userEmail);
        int check = 0;
        Statement stmt = null;
        ResultSet rset;
        ArrayList<String> retval = new ArrayList<>();
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            while (rset.next()) {
                retval.add(rset.getString("followee"));
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public ArrayList<String> getFollowerList(String userEmail) {
        String sqlStmt = String.format("SELECT follower FROM follow_list WHERE followee=\"%s\";", userEmail);
        int check = 0;
        Statement stmt = null;
        ResultSet rset;
        ArrayList<String> retval = new ArrayList<>();
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            while (rset.next()) {
                retval.add(rset.getString("follower"));
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public int getNumOfFollowers(String userEmail) {
        return getFollowerList(userEmail).size();
    }

    public int getNumberOfFollowees(String userEmail) {
        return getFollowingList(userEmail).size();
    }
}

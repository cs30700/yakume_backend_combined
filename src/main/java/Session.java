import java.security.SecureRandom;
import java.util.Base64;

/**
 * User's Session Cookie class
 * Note: We DO NOT save session cookie information in the database.
 * Users have to login again if service is restarted.
 * Created by yuxuan on 8/23/16.
 */
public class Session {
    private static final int COOKIE_LEN = 32;

    private static long validityPeriod = Config.config.cookie_validity_sec * 1000 /*milliseconds in a second*/;
    private String sessionCookieIntegrityCheckCode = null;
    private long timestampIssued = 0;
    private long timestampExpiring = 0;

    public Session() {
        Base64.Encoder b64_encode = Base64.getEncoder();
        SecureRandom sr;
        byte[] randomBytes = new byte[32];
        try {
            sr = SecureRandom.getInstance("SHA1PRNG");
            sr.nextBytes(randomBytes);
        } catch (Exception e) {
            System.out.println("FATAL ERROR: " + e.toString());
        }

        sessionCookieIntegrityCheckCode = b64_encode.encodeToString(randomBytes);
        timestampIssued = System.currentTimeMillis();
        timestampExpiring = timestampIssued + validityPeriod;
    }

    public String getSessionCookieIntegrityCheckCode() {
        return sessionCookieIntegrityCheckCode;
    }

    public long getTimestampIssued() {
        return timestampIssued;
    }

    /**
     * check if the cookie is still valid.
     * @param cookieID
     * @return true if cookie is valid.
     */
    public boolean isValid(String cookieID) {
        if (sessionCookieIntegrityCheckCode == null) {
            return false;
        }
        if (!sessionCookieIntegrityCheckCode.equals(cookieID)) {
            return false;
        }
        if (System.currentTimeMillis() > timestampExpiring) {
            return false;
        }
        return true;
    }

    /**
     * test if cookie has expired.
     * @return true if cookie is valid.
     */
    public boolean isValid()
    {
        if (sessionCookieIntegrityCheckCode == null) {
            return false;
        }
        if (System.currentTimeMillis() > timestampExpiring) {
            return false;
        }
        return true;
    }

    public boolean renew() {
        if (isValid()) {
            timestampExpiring = System.currentTimeMillis() + validityPeriod;
            return true;
        }
        return false;
    }

    public void invalidate() {
        timestampExpiring = 0;
    }
}
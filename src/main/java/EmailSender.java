/**
 * Reference from: http://crunchify.com/java-mailapi-example-send-an-email-via-gmail-smtp/
 */

/**
 *
 Outgoing Mail (SMTP) Server:
     smtp.gmail.com
     Requires SSL: Yes
     Requires TLS: Yes (if available)
     Requires Authentication: Yes
     Port for SSL: 465
     Port for TLS/STARTTLS: 587
 */

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailSender implements Runnable
{
    private String toAddress;
    private String subject;
    private String body;
    private static boolean setupDone = false;

    private static Properties mailServerProperties;

    public EmailSender(String toAddress, String subject, String body) {
        if (!setupDone) {
            mailServerProperties = System.getProperties();
            mailServerProperties.put("mail.smtp.port", "587");
            mailServerProperties.put("mail.smtp.auth", "true");
            mailServerProperties.put("mail.smtp.starttls.enable", "true");
            setupDone = true;
        }
        this.toAddress = toAddress;
        this.subject = subject;
        this.body = body;
    }

    @Override
    public void run() {
        try {
            MimeMessage generateMailMessage;
            Session getMailSession = Session.getDefaultInstance(mailServerProperties, null);
            generateMailMessage = new MimeMessage(getMailSession);
            generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
            generateMailMessage.setSubject(subject);
            generateMailMessage.setContent(body, "text/html");
            Transport transport = getMailSession.getTransport("smtp");
            transport.connect("smtp.gmail.com", Config.config.email_addr, Config.config.email_pass);
            transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
            transport.close();
        } catch (MessagingException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
        }
    }
}

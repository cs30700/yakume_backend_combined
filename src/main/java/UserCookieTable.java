//Unused

import java.sql.SQLException;

/**
 * Created by yuxuan on 10/4/16.
 */
public class UserCookieTable
{
    SQLDBConnection dbConn;

    public UserCookieTable(SQLDBConnection dbConn) throws SQLException {
        this.dbConn = dbConn;
        this.dbConn.connect();
    }

    synchronized public void put(String userEmail, Session cookie) {

    }

    public Session getCookie(String userEmail) {
        return null;
    }

    public boolean validate(String userEmail, String cookieID) {
        return false;
    }

    synchronized public void logout(String userEmail, String cookieID) {

    }

    synchronized public void renew(String userEmail, String cookieID) {

    }
}

all:
	@mvn package
	@git add -A
	@git commit -m "Auto commit with make on `date`"

deploy:
	@printf "[Warning] Deploy only with: \n\t\t(0)rebuilt artifects\n\t\t(1)working code\n"
	@./confirm
	@rm -rf deploy/
	@rm -f latest_production
	@mkdir deploy
	@cp yakume.db deploy/
	@cp server.conf deploy/
	@cp startup.sh deploy/
	@cp target/yakume-server-1.0-jar-with-dependencies.jar deploy/yakume-server.jar
	@cp out/artifacts/Yakume_jar/Yakume.jar deploy/yakume-intellij.jar
	@echo "production-`date +%s`" > latest_production
	@mv deploy `cat latest_production`
	@scp -r `cat latest_production` ychen@yakume.xyz:~
	@scp -r latest_production ychen@yakume.xyz:~

clean:
	@rm -rf production-* latest_production

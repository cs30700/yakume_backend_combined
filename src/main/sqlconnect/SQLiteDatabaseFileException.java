/**
 * Created by yuxuan on 8/26/16.
 */
public class SQLiteDatabaseFileException extends Exception {
    public SQLiteDatabaseFileException(String message) {
        super(message);
    }
}

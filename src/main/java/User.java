import org.kopitubruk.util.json.JsonObject;

import javax.servlet.http.Cookie;

/**
 * DB Schema
 * CREATE TABLE users(
     name VARCHAR(1024),
     email VARCHAR(1024) PRIMARY KEY,
     password VARCHAR(1024),
     fb_token VARCHAR(1024),
     verified INT NULL,
     city VARCHAR(512),
     description VARCHAR(1024)
   );
*/

/**
 * Created by yuxuan on 6/8/16.
 */
public class User
{
    private String name;
    private String email;
    private Password securePassword;
    private Session cookie;
    private String FacebookLoginToken;
    private String city;
    private String description;

    public User() {
        name = "";
        email = "";
        securePassword = null;
        cookie = null;
        FacebookLoginToken = "";
    }

    public void setEmail(String email) {
        if (email != null)
            this.email = email;
    }

    public String getSecurePassword() {
        if (securePassword == null) {
            return "";
        }
        return securePassword.serialize();
    }

    public void setSecurePassword(Password securePassword) {
        this.securePassword = securePassword;
    }

    public String getName() {
        return name;
    }

    public static User createUser(String name, String email, char[] password) {
        if (email == null || email.isEmpty()) {
            return null;
        }
        if (name == null || name.isEmpty()) {
            name = "noname";
        }
        User retval = new User();
        retval.name = name;
        retval.email = email;
        try {
            retval.securePassword = Password.createPassword(password);
        } catch (Exception e) {
            return null;
        }
        return retval;
    }

    @Override
    public String toString() {
        return toJsonObject().toJSON();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof User)) {
            return false;
        }
        User u = (User)o;
        return u.email.equals(email);
    }

    public boolean login(char[] pass) {
        boolean success = false;
        try {
            success = securePassword.checkPassword(pass);
        } catch (Exception e) {
            System.out.println("user " + email + " login fail due to " + e.toString());
            return false;
        } finally {
            for (int i = 0; i < pass.length; i++) {
                pass[i] = 0;
            }
        }

        if (success) {
            cookie = new Session();
        }
        return success;
    }

    public void internalLogin() {
        cookie = new Session();
    }

    public String getCookie() {
        if (cookie == null) {
            return null;
        }

        if (!cookie.isValid()) {
            cookie = null;
            return null;
        }

        cookie.renew();
        return cookie.getSessionCookieIntegrityCheckCode();
    }

    Session getCookieObject() {
        return cookie;
    }

    public boolean verifySession(String sessionID) {
        if (cookie == null) {
            return false;
        }
        if (cookie.isValid(sessionID)) {
            return true;
        }
        cookie.invalidate();
        return false;
    }

    public String getSerializedSecurePassword() {
        return securePassword.serialize();
    }

    void setSecurePassword(String pass) {
        securePassword = Password.deserialize(pass);
    }

    public String getFacebookLoginToken() {
        return FacebookLoginToken;
    }

    public void setFacebookLoginToken(String facebookLoginToken) {
        FacebookLoginToken = facebookLoginToken;
    }

    public void setCookie(Session cookie) {
        this.cookie = cookie;
    }

    public String getEmail() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JsonObject toJsonObject() {
        return new JsonObject()
                .add("name", name)
                .add("email", email)
                .add("city", city)
                .add("description", description);
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * DB Schema:
 * CREATE TABLE avatar(
 *      user_email VARCHAR(1024) PRIMARY KEY,
 *      file_name VARCHAR(1024)
 * );
 */
public class AvatarOwnershipTable
{
    SQLDBConnection dbConn;

    public AvatarOwnershipTable(SQLDBConnection dbConn) throws SQLException {
        this.dbConn = dbConn;
        this.dbConn.connect();
    }

    public int updateAvatar(String userEmail, String fileName) {
        //change or add for the first time
        deleteAvatar(userEmail);
        String sqlStmt = String.format("INSERT INTO avatar VALUES(\"%s\", \"%s\")", userEmail, fileName);
        int check = 0;
        try {
            check = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check == 1 ? 0 : 1;
    }

    public int deleteAvatar(String userEmail) {
        if (userEmail == null) {
            return 1;
        }
        String sqlStmt = String.format("DELETE FROM avatar WHERE user_email=\"%s\";", userEmail);
        try {
            dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public String getAvatarFilename(String userEmail) {
        if (userEmail == null) {
            return null;
        }
        String sqlStmt = String.format("SELECT * FROM avatar WHERE user_email=\"%s\";", userEmail);
        Statement stmt = null;
        ResultSet rset;
        String retval = null;
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            if (rset.next()) {
                retval = rset.getString("file_name");
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }
}

import com.owlike.genson.*;

/**
 * Created by yuxuan on 10/11/16.
 */
public class TestJsonObject
{
    public static void main(String[] args) {
        String json = "    {\n" +
                "        \"data\": {\n" +
                "            \"app_id\": 138483919580948, \n" +
                "            \"application\": \"Social Cafe\", \n" +
                "            \"expires_at\": 1352419328, \n" +
                "            \"is_valid\": true, \n" +
                "            \"issued_at\": 1347235328, \n" +
                "            \"metadata\": {\n" +
                "                \"sso\": \"iphone-safari\"\n" +
                "            }, \n" +
                "            \"scopes\": [\n" +
                "                \"email\", \n" +
                "                \"publish_actions\"\n" +
                "            ], \n" +
                "            \"user_id\": 1207059\n" +
                "        }\n" +
                "    }";

        Genson mygenson = new Genson();
        FacebookResponse fr = mygenson.deserialize(json, FacebookResponse.class);

    }



}

/**
 * Created by yuxuan on 9/30/16.
 */
public class Location {
    int zipcode;
    String address;
    double longitude;
    double latitude;

    public Location(int zipcode, String address, double longitude, double latitude) {
        this.zipcode = zipcode;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
    }
}

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * DB Schema:
 * CREATE TABLE event_img(
 *     eventid INTEGER NOT NULL,
 *     file_name VARCHAR(1024)
 * );
 */

public class ImageOwnershipTable
{
    SQLDBConnection dbConn;

    public ImageOwnershipTable(SQLDBConnection dbConn) throws SQLException {
        this.dbConn = dbConn;
        this.dbConn.connect();
    }

    public int addImageToEvent(int eventId, String fileName) {
        String sqlStmt = String.format("INSERT INTO event_img VALUES(%d, \"%s\");", eventId, fileName);
        int check = 0;
        try {
            check = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check == 1 ? 0 : 1;
    }

    public int removeImageFromEvent(int eventId, String fileName) {
        String sqlStmt = String.format("DELETE FROM event_img WHERE eventid=%d AND file_name=\"%s\";", eventId, fileName);
        int check = 0;
        try {
            check = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check == 1 ? 0 : 1;
    }

    public int removeAllImagesFromEvent(int eventId) {
        String sqlStmt = String.format("DELETE FROM event_img WHERE eventid=%d;", eventId);
        int check = 0;
        try {
            check = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check == 1 ? 0 : 1;
    }

    public ArrayList<String> getImageFilenames(int eventId) {
        ArrayList<String> retval = new ArrayList<>();
        String sqlStmt = String.format("SELECT * FROM event_img WHERE eventid=%d;", eventId);
        Statement stmt = null;
        ResultSet rset;
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            while (rset.next()) {
                retval.add(rset.getString("file_name"));
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }
}

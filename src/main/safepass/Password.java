import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.Base64;

/**
 * Project Yakume
 * Password Class
 * Author: Yuxuan Chen
 * Securely store a password.
 *
 * Tutorials on encryption and salt-hashing is at:
 * http://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
 */
public class Password
{
    /**
     * Parameters for encryption,
     * should not be changed unless you want all users reset password.
     */
    private static final int SALT_LEN = 32;
    private static final String ALGORITHM = "SHA-256";
    private static final String MAGIC = "<<<sAfEpAsS>>>";

    /**
     * Actual part of password
     * random_sequence is a salt
     * encoded_bytes is encrypted password
     */
    private byte[] random_sequence;
    private byte[] encoded_bytes;

    /**
     * A dummy private constructor
     * Use createPassword to create a new instance
     */
    private Password() {

    }

    /**
     * Private constructor to generate salt and encrypt the password
     * @param passwordInClearText
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    private Password(byte[] passwordInClearText) throws NoSuchAlgorithmException, NoSuchProviderException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
        random_sequence = new byte[SALT_LEN];
        sr.nextBytes(random_sequence);
        MessageDigest md = MessageDigest.getInstance(ALGORITHM);
        md.update(random_sequence);
        encoded_bytes = md.digest(passwordInClearText);
    }

    /**
     * Public method for creating a password
     * @param passwordInClearText
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static Password createPassword(char[] passwordInClearText) {
        byte[] password = new byte[passwordInClearText.length];
        for (int i = 0; i < passwordInClearText.length; i++) {
            password[i] = (byte)passwordInClearText[i];
            passwordInClearText[i] = 0;
        }
        Password retval = null;
        try {
            retval = new Password(password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < password.length; i++) {
            password[i] = 0;
        }
        return retval;
    }

    /**
     * Check a password correctness by performing same operation
     * @param passwordInClearText
     * @return
     * @throws NoSuchAlgorithmException
     */
    public boolean checkPassword(char[] passwordInClearText) throws NoSuchAlgorithmException {
        byte[] password = new byte[passwordInClearText.length];
        for (int i = 0; i < passwordInClearText.length; i++) {
            password[i] = (byte)passwordInClearText[i];
            passwordInClearText[i] = 0;
        }
        MessageDigest md = MessageDigest.getInstance(ALGORITHM);
        md.update(random_sequence);
        byte[] temp = md.digest(password);
        if (temp.length != encoded_bytes.length) {
            return false;
        }
        for (int i = 0; i < temp.length; i++) {
            if (temp[i] != encoded_bytes[i]) {
                return false;
            }
        }
        return true;
    }


    /**
     * Export exactly same instance
     * @return
     */
    public String serialize() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < SALT_LEN; i++) {
            str.append((char)random_sequence[i]);
        }
        str.append(MAGIC);
        for (int i = 0; i < encoded_bytes.length; i++) {
            str.append((char)encoded_bytes[i]);
        }
        byte[] before_encoding = str.toString().getBytes();
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(before_encoding);
    }

    /**
     * Import an instance, useful for database.
     * @param base64 safePass
     * @return
     */
    public static Password deserialize(String base64) {
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] decoded = decoder.decode(base64);
        String safePass = new String(decoded);
        String[] parsed = safePass.split(MAGIC);
        if (parsed.length != 2) {
            return null;
        }
        if (parsed[0].length() != SALT_LEN) {
            return null;
        }
        Password retval = new Password();
        byte[] salt = new byte[SALT_LEN];
        for (int i = 0; i < SALT_LEN; i++) {
            salt[i] = (byte)parsed[0].charAt(i);
        }
        retval.random_sequence = salt;
        byte[] encoded_pass = new byte[parsed[1].length()];
        for (int i = 0; i < encoded_pass.length; i++) {
            encoded_pass[i] = (byte)parsed[1].charAt(i);
        }
        retval.encoded_bytes = encoded_pass;
        return retval;
    }
}

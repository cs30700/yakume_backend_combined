import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Scanner;

/**
 * Created by yuxuan on 9/13/16.
 */
public class TestPassword {
    public static void main(String[] args) throws NoSuchProviderException, NoSuchAlgorithmException {
        System.out.print("Please set password: ");
        Scanner myScanner = new Scanner(System.in);
        String password = myScanner.nextLine();
        System.out.print("Please type in a second time: ");
        String password_2nd = myScanner.nextLine();
        Password mypass = Password.createPassword(password.toCharArray());
        System.out.println(mypass.checkPassword(password_2nd.toCharArray()) ? "match" : "do not match");
        String serialized = mypass.serialize();
        //System.out.println(serialized);
        Password deserialized = Password.deserialize(serialized);
        if (deserialized == null) {
            System.out.println("FATAL ERROR: deserialize does not work.");
            System.exit(1);
        }
        System.out.println(deserialized.checkPassword(password_2nd.toCharArray()) ? "match" : "do not match");
        System.out.println("Encrypted form: " + serialized);
        System.out.println("Encrypted length: " + serialized.length());
    }
}

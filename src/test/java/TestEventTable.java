import junit.framework.TestCase;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Created by yuxuan on 10/9/16.
 */
public class TestEventTable extends TestCase
{
    EventTable et;
    SQLDBConnection dbConn;
    @Override
    protected void setUp() throws Exception {
        dbConn = SQLiteConnection.createConnection("yakume.db");
        et = new EventTable(dbConn);
    }

    @Test
    public void testAddEvent() {
        Event myEv = new Event(System.currentTimeMillis() + 1000000, new Location(47906, "138 S Chauncey Ave", 1.1, 2.2), "Hello", "World", null, 100, "hello@wor.ld");
        int id = et.putEvent(myEv);
        Event new_ev = et.findEventById(id);
        assert new_ev != null;
    }

    public void testModifyEvent() {
        Event myEv = new Event();
        int id = et.putEvent(myEv);
        myEv.setTitle("Hello");
        et.modifyEvent(id, myEv);
        assert et.findEventById(id).getTitle().equals("Hello");
    }
}

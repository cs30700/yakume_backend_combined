import java.io.File;
import java.sql.*;
/**
 * Created by yuxuan on 8/26/16.
 */
public class SQLiteConnection implements SQLDBConnection
{
    private String database_filename;
    private Connection connection;
    private boolean connected = false;
    /**
     * Constructor is made private
     * We would like to use the static factory mode.
     */
    private SQLiteConnection()
    {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            throw new SQLiteJDBCClassNotFoundError("Please run with sqlite-jdbc jar file.");
        }
        database_filename = null;
        connection = null;
    }

    /**
     * SQL connection factory.
     * Create connection with decent amount of checking.
     * @param database_filename
     * @return
     * @throws SQLiteDatabaseFileException
     */
    public static SQLiteConnection createConnection(String database_filename) throws SQLiteDatabaseFileException {
        if (database_filename == null) {
            return null;
        }
        SQLiteConnection retval = new SQLiteConnection();
        File database_file = new File(database_filename);
        if (!database_file.exists()) {
            throw new SQLiteDatabaseFileException("Database file " + database_filename + " does not exist.");
        }
        retval.database_filename = database_filename;
        return retval;
    }

    /**
     * connects to the actual database
     * The DB is not automatically opened.
     */
    public void connect() throws SQLException {
        if (!connected) {
            connection = DriverManager.getConnection("jdbc:sqlite:" + database_filename);
            connected = true;
        }
    }

    /**
     * Atomic method to execute queries
     * @param sqlQueryString
     * @return
     * @throws SQLException
     */
    public synchronized Statement getStatement() throws SQLException {
        Statement stmt = connection.createStatement();
        return stmt;
    }

    /**
     * Atomic method to execute updates
     * @param sqlUpdateString
     * @return
     * @throws SQLException
     */

    public synchronized int update(String sqlUpdateString) throws SQLException {
        Statement stmt = connection.createStatement();
        int retval = stmt.executeUpdate(sqlUpdateString);
        stmt.close();
        return retval;
    }

    /**
     * Graceful shutdown this connection
     * This object can be preserved and used later.
     * @throws SQLException
     */
    public void close() throws SQLException {
        if (connected) {
            connection.close();
            connection = null;
            connected = false;
        }
    }
}

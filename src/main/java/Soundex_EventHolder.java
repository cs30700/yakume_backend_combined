import java.util.*;

public class Soundex_EventHolder {
    BitSet[] events;
    String[][] titles;
    Soundex soundexer;
    int maxevents;

    // Constructor.
    public Soundex_EventHolder(int soundmax, int maxevents) {
        this.soundexer = new Soundex(soundmax);
        this.maxevents = maxevents;
        events = new BitSet[maxevents];
        titles = new String[maxevents][];
    }

    // Enlarge the event array.
    public void enlarge() {
        int new_maxevents = 2 * maxevents;
        BitSet[] new_events = new BitSet[new_maxevents];
        String[][] new_titles = new String[new_maxevents][];
        int i = 0;
        while (i < maxevents) {
            new_events[i] = events[i];
            new_titles[i] = titles[i];
            i++;
        }
        maxevents = new_maxevents;
        events = new_events;
        titles = new_titles;
    }

    // Push an event with id and title.
    public void write(int id, String title) {
        while (id >= maxevents) {
            enlarge();
        }

        // Add soundex
        events[id] = soundexer.create(title);

        // Add string
        titles[id] = title.split(" ");

        int len = titles[id].length;
        int i = 0;
        while (i < len) {
            titles[id][i] = Soundex.fix_word(titles[id][i]);
            i++;
        }
    }

    // See if an event has the soundex of word.
    public boolean has_soundex(int id, String word) {
        while (id >= maxevents) {
            enlarge();
        }
        if (events[id] == null) {
            return false;
        }
        int index = Soundex.get_index(soundexer.convert_word(word));
        return (events[id].get(index) == true);
    }

    // See if an event has the string of word. Linear search.
    public boolean has_string(int id, String word) {
        while (id >= maxevents) {
            enlarge();
        }
        if (titles[id] == null) {
            return false;
        }

        word = Soundex.fix_word(word);
        int len = titles[id].length;
        int i = 0;
        while (i < len) {
            if (word.equals(titles[id][i])) {
                return true;
            }
            i++;
        }

        return false;
    }

    // Return a sorted array of event id.
    public int[] read(String title) {
        String[] words = title.split(" ");

        // Fix words.
        int len_words = words.length;
        int cyc_words = 0;
        while (cyc_words < len_words) {
            words[cyc_words] = Soundex.fix_word(words[cyc_words]);
            cyc_words++;
        }

        // Calculate value of events.
        int len_events = events.length;
        int cyc_events = 0;
        int[] val_events = new int[len_events];
        while (cyc_events < len_events) {
            cyc_words = 0;
            while (cyc_words < len_words) {
                if (has_soundex(cyc_events, words[cyc_words])) {
                    val_events[cyc_events] += 2;
                }
                if (has_string(cyc_events, words[cyc_words])) {
                    val_events[cyc_events] += 3;
                }
                cyc_words++;
            }
            cyc_events++;
        }

        // Generate order.
        int[] ord_events = new int[len_events];
        cyc_events = 0;
        while (cyc_events < len_events) {
            ord_events[cyc_events] = cyc_events;
            cyc_events++;
        }

        // Sort.
        sort_val(val_events, ord_events, 0, len_events - 1);
        return ord_events;
    }

    // quick sort.
    public static void sort_val(int[] val, int[] ord, int st, int ed) {

        if (st >= ed) {
            return;
        }

        int l = st;
        int r = ed;
        int temp;
        boolean f = true;

        while (l < r) {
            if (f) {
                while ((l < r) && (val[ord[l]]) >= val[ord[r]]) {
                    l++;
                }
            } else {
                while ((l < r) && (val[ord[l]]) >= val[ord[r]]) {
                    r--;
                }
            }
            if (l < r) {
                temp = ord[l];
                ord[l] = ord[r];
                ord[r] = temp;
                f = !f;
            }
        }

        sort_val(val, ord, st, l - 1);
        sort_val(val, ord, r + 1, ed);
    }

    // Test method.
    public static void print_array(int[] array) {
        int l = array.length;
        int i = 0;
        while (i < l) {
            System.out.print(String.format("%d ", array[i]));
            i++;
        }
        System.out.print("\n");
    }

    public static void main(String[] args) {
        Soundex_EventHolder s = new Soundex_EventHolder(3, 5);
        s.write(0, "J I H G F");
        s.write(1, "A B C D E");
        s.write(2, "A B C G F");
        s.write(3, "A B C D F");
        s.write(4, "A I H G F");
        s.write(5, "A B H G F");
        int[] result = s.read("E D C B A");
        print_array(result);
    }
}

class Soundex {

    private int soundmax;

    public Soundex(int soundmax) {
        this.soundmax = soundmax;
    }

    // Size of a bitset with certain soundmax.
    public static int size(int max) {
        int current = 26;
        int i = 0;
        while (i < max) {
            current = current * 7;
            i++;
        }
        return current + 1;
    }

    // Return the index of a soundex.
    public static int get_index(int[] table) {
        int length = table.length;
        while (length > 0 && table[length - 1] < 0) {
            length--;
        }
        if (length == 0) {
            return 0;
        }
        int current = 0;
        int i = 0;
        while (i < length) {
            current = current * 7 + table[i];
            i++;
        }
        return current + 1;
    }

    // Return the soundex of a index.
    public static int[] get_soundex(int index) {
        int[] table;
        if (index == 0) {
            table = new int[1];
            table[0] = -1;
            return table;
        }
        index = index - 1;
        int t = index;
        int len = 1;
        while (t >= 26) {
            t = t / 7;
            len++;
        }
        table = new int[len];
        while (index >= 26) {
            len--;
            table[len] = index % 7;
            index = index / 7;
        }
        table[0] = index;
        return table;
    }

    // See if a char is valid.
    public static int fix_char(char c) {
        if (c >= 'A' && c <= 'Z') {
            c = (char) (c + 'a' - 'A');
        }
        if (c >= 'a' && c <= 'z') {
            return c;
        }
        return -1;
    }

    // Convert a char to a soundex. 0 otherwise.
    public static int convert_char(char p) {
        int c = fix_char(p);
        if (c < 0) {
            return 0;
        }
        if (c == 'b' || c == 'f' || c == 'p' || c == 'v') {
            return 1;
        }
        if (c == 'c' || c == 'g' || c == 'j' || c == 'k' || c == 'q' || c == 's' || c == 'x' || c == 'z') {
            return 2;
        }
        if (c == 'd' || c == 't') {
            return 3;
        }
        if (c == 'l') {
            return 4;
        }
        if (c == 'm' || c == 'n') {
            return 5;
        }
        if (c == 'r') {
            return 6;
        }
        return 0;
    }

    // See if a char is valid.
    public static String fix_word(String ini) {
        String fin = new String();
        int len = ini.length();
        int i = 0;
        int c;
        while (i < len) {
            c = fix_char(ini.charAt(i));
            if (c >= 0) {
                fin = fin.concat(Character.toString((char) c));
            }
            i++;
        }
        return fin;
    }

    // Convert a word to a soundex array. The first is the leading character, followed by "soundmex" number of soundex.
    public int[] convert_word(String s) {
        int[] ans = new int[soundmax + 1];
        int j = 0;
        while (j < soundmax + 1) {
            ans[j] = -1;
            j++;
        }

        int len = s.length();
        if (len == 0) {
            return ans;
        }

        // Find the first character to be the leader.
        int c;
        int i = 0;
        while (i < len && ans[0] < 0) {
            c = fix_char(s.charAt(i));
            if (c > 0) {
                ans[0] = c - 'a';
            }
            i++;
        }

        j = 0;
        while (i < len && j < soundmax) {
            while (i < len && ans[j + 1] < 0) {
                c = convert_char(s.charAt(i));
                if (c > 0) {
                    ans[j + 1] = c;
                }
                i++;
            }
            j++;
        }
        return ans;
    }

    // Create bitset from a long title.
    public BitSet create(String title) {
        int length = size(soundmax);
        BitSet ans = new BitSet(length);
        ans.clear();

        String[] words = title.split(" ");
        int numwords = words.length;
        int i = 0;
        while (i < numwords) {
            ans.set(get_index(convert_word(words[i])));
            i++;
        }
        return ans;
    }
}
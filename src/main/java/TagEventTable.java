/**
 * Created by yuxuan on 10/10/16.
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * DB Schema
 CREATE TABLE event_tags(
     eventid int,
     tag VARCHAR(1024)
 );
 */
public class TagEventTable
{
    SQLDBConnection dbConn;

    public TagEventTable(SQLDBConnection dbConn) {
        this.dbConn = dbConn;
        try {
            dbConn.connect();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean contains(int eventId, String tag) {
        String sqlStmt = String.format("SELECT 1 FROM event_tags WHERE eventid=%d AND tag=\"%s\";", eventId, tag);
        Statement stmt = null;
        ResultSet rset = null;
        try {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            if (rset.next()) {
                return true;
            }
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    YakumeServer.logger.print(e.toString());
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public int addEntry(int eventId, String tag) {
        if (contains(eventId, tag)) {
            return -2;
        }
        String sqlStmt = String.format("INSERT INTO event_tags VALUES(%d, \"%s\");", eventId, tag);
        int check_val = -1;
        try {
            check_val = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return -1;
        }
        return check_val == 1 ? 0 : 1;
    }

    public void addTagsForEvent(int eventId, ArrayList<String> tags) {
        for (int i = 0; i < tags.size(); i++) {
            addEntry(eventId, tags.get(i));
        }
    }

    public ArrayList<Integer> getEventsByTag(String tag) {
        if (tag == null) {
            return null;
        }
        String sqlStmt = String.format("SELECT eventid FROM event_tags WHERE tag=\"%s\";", tag);
        ArrayList<Integer> retval = new ArrayList<>();
        Statement stmt = null;
        ResultSet rset = null;
        try {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            while (rset.next()) {
                retval.add(rset.getInt("eventid"));
            }
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    YakumeServer.logger.print(e.toString());
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public ArrayList<String> getTagsByEventID(int eventId) {
        String sqlStmt = String.format("SELECT tag FROM event_tags WHERE eventid=%d;", eventId);
        ArrayList<String> retval = new ArrayList<>();
        Statement stmt = null;
        ResultSet rset = null;
        try {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            while (rset.next()) {
                retval.add(rset.getString("tag"));
            }
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    YakumeServer.logger.print(e.toString());
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public int deleteEntry(int eventId, String tag) {
        String sqlStmt = String.format("DELETE FROM event_tags WHERE eventid=%d AND tag=\"%s\";", eventId, tag);
        int check_val = -1;
        try {
            check_val = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return -1;
        }
        return check_val;
    }

    public int deleteEventFromAll(int eventId) {
        String sqlStmt = String.format("DELETE FROM event_tags WHERE eventid=%d;", eventId);
        int check_val = -1;
        try {
            check_val = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return -1;
        }
        return check_val;
    }
}

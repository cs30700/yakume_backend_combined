import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;

/**
 * Created by yuxuan on 10/23/16.
 */

/**
 * DB Schema
 * CREATE TABLE timeline_entry(
     time LONG,
     email VARCHAR(256),
     action VARCHAR(256),
     eventid INT
   );
 */
public class TimelineTable
{
    SQLDBConnection dbConn;

    public TimelineTable(SQLDBConnection dbConn) {
        this.dbConn = dbConn;
    }

    public int addEntry(TimelineEntry te) {
        String sqlStmt = String.format("INSERT INTO timeline_entry VALUES(%d, \"%s\", \"%s\", %d);",
                te.time, te.userEmail, te.action, te.event);
        int check = 0;
        try {
            check = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
            return 1;
        }
        return check == 1 ? 0 : 1;
    }

    public int deleteEvent(int eventId) {
        String sqlStmt = String.format("DELETE FROM timeline_entry WHERE eventid=%d;", eventId);
        int check = 0;
        try {
            check = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
            return 1;
        }
        return check == 1 ? 0 : 1;
    }

    public ArrayList<TimelineEntry> getTimeLineEntries(long startTime, long endTime, String userEmail) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM timeline_entry WHERE 1 ");
        if (startTime >= 0) {
            query.append(String.format("AND time >= %d ", startTime));
        }
        if (endTime >= 0) {
            query.append(String.format("AND time <= %d ", endTime));
        }
        if (userEmail != null) {
            query.append(String.format("AND email = \"%s\" ", userEmail));
        }
        query.append(";");
        Statement stmt = null;
        ResultSet rset = null;
        ArrayList<TimelineEntry> retval = new ArrayList<>();
        try {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(query.toString());
            while (rset.next()) {
                TimelineEntry toadd = new TimelineEntry();
                toadd.time = (rset.getLong("time"));
                toadd.userEmail = (rset.getString("email"));
                toadd.action = rset.getString("action");
                toadd.event = rset.getInt("eventid");
                retval.add(toadd);
            }
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return null;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    YakumeServer.logger.print(e.toString());
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }
}

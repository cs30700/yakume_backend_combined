import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by yuxuan on 10/1/16.
 */
interface SQLDBConnection {
    void connect() throws SQLException;
    Statement getStatement() throws SQLException;
    int update(String sqlUpdateString) throws SQLException;
    void close() throws SQLException;
}

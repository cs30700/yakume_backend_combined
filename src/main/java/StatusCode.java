import org.kopitubruk.util.json.JsonObject;

/**
 * Created by yuxuan on 10/10/16.
 */
public class StatusCode
{
    public static final String SUCCESS = "SUCCESS";
    public static final String ERR_USER_NOT_FOUND = "ERR_USER_NOT_FOUND";
    public static final String ERR_USER_EMAIL_TAKEN = "ERR_USER_EMAIL_TAKEN";
    public static final String ERR_PASSWORD_INCORRECT = "ERR_PASSWORD_INCORRECT";
    public static final String ERR_INVALID_ARGUMENT = "ERR_INVALID_ARGUMENT";
    public static final String ERR_EVENT_ID_NOT_FOUND = "ERR_EVENT_ID_NOT_FOUND";
    public static final String ERR_NOT_LOGGED_IN = "ERR_NOT_LOGGED_IN";
    public static final String ERR_ALREADY_REGISTERED = "ERR_ALREADY_REGISTERED";
    public static final String ERR_NOT_YET_REGISTERED = "ERR_NOT_YET_REGISTERED";
    public static final String ERR_PERMISSION_DENIED = "ERR_PERMISSION_DENIED";
    public static final String ERR_EXCEPTION_THROWN = "ERR_EXCEPTION_THROWN";
    public static final String ERR_INVALID_COOKIES = "ERR_INVALID_COOKIES";
    public static final String ERR_NUM_FORMAT = "ERR_NUM_FORMAT";
    public static final String ERR_FACEBOOK_EMAIL_NOT_SET = "ERR_FACEBOOK_EMAIL_NOT_SET";
    public static final String ERR_RECAPTCHA_INVALID = "ERR_RECAPTCHA_INVALID";

    public static String loginSuccess(String sessionID) {
        return new JsonObject()
                .add("status", "success")
                .add("sessionID", sessionID)
                .toJSON();
    }

    public static String generateErrorCode(String status) {
        return new JsonObject()
                .add("status", "fail")
                .add("errorcode", status)
                .toJSON();
    }

    public static String success() {
        return new JsonObject()
                .add("status", "success")
                .toJSON();
    }
}

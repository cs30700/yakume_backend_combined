import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Formatter;
import java.util.Objects;

/**
 * Created by yuxuan on 6/27/16.
 */
public class Log
{
    long programCreateTime;
    File logFileLocation;
    PrintWriter pw;
    public Log(String logFilePath) {
        this.programCreateTime = System.currentTimeMillis();
        this.logFileLocation = new File(logFilePath);
        try {
            pw = new PrintWriter(logFileLocation);
        } catch (FileNotFoundException e) {
            System.out.println("Log file not found");
        }
    }

    long uptime()
    {
        return System.currentTimeMillis() - programCreateTime;
    }

    double uptime_seconds() {
        return (double)uptime() / 1000;
    }

    public void error(String format, Object... args) {
        System.err.print(String.format("[%f] ", uptime_seconds()));
        String s = new Formatter().format(format, args).toString();
        System.err.println(s);
        pw.print(String.format("[%f] %s\n", uptime_seconds(), s));
        pw.flush();
    }

    public void print(String format, Object... args) {
        System.out.print(String.format("[%f] ", uptime_seconds()));
        String s = new Formatter().format(format, args).toString();
        System.out.println(s); //Make sure the log is flushed.

        pw.print(String.format("[%f] %s\n", uptime_seconds(), s));
        pw.flush();
    }

}

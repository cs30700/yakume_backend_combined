/**
 * Created by yuxuan on 11/6/16.
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Base64;
import java.util.UUID;

/**
 * CREATE TABLE payment_tokens(
     token VARCHAR(1024),
     amount INT,
     used INT
   );
 */

public class PaymentTokensTable {
    SQLDBConnection dbConn;

    public PaymentTokensTable(SQLDBConnection dbConn) throws SQLException {
        this.dbConn = dbConn;
        dbConn.connect();
    }

    public String createToken(int amount) {
        Base64.Encoder encoder = Base64.getEncoder();
        String token = UUID.randomUUID().toString();
        String retval = encoder.encodeToString(token.getBytes());
        String sqlStmt = String.format("INSERT INTO payment_tokens VALUES(\"%s\", %d, %d);", retval, amount, 0);
        try {
            dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return retval;
    }

    public int validateToken(String token, int amount) {
        if (isValid(token) != 0) {
            return 2;
        }
        if (getAmount(token) != amount) {
            return 1;
        }
        String sqlStmt = String.format("DELETE FROM payment_tokens WHERE token=\"%s\";", token);
        try
        {
            dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getAmount(String token) {
        String sqlStmt = String.format("SELECT * FROM payment_tokens WHERE token=\"%s\";", token);
        Statement stmt = null;
        ResultSet rset = null;
        int retval = 0;
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            if (rset.next()) {
                retval = rset.getInt("amount");
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public int isValid(String token) {
        String sqlStmt = String.format("SELECT used FROM payment_tokens WHERE token=\"%s\";", token);
        Statement stmt = null;
        ResultSet rset = null;
        int retval = 1;
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            if (rset.next()) {
                if (rset.getInt("used") == 0) {
                    retval = 0;
                }
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }
}

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by yuxuan on 10/1/16.
 */

/**
 * DB Schema
     CREATE TABLE users(
         name VARCHAR(1024),
         email VARCHAR(1024) PRIMARY KEY,
         password VARCHAR(1024),
         fb_token VARCHAR(1024),
         verified INT NULL,
         city VARCHAR(512),
         description VARCHAR(1024)
     );
 */
public class UserTable {

    private SQLDBConnection dbConn;

    private ConcurrentHashMap<String, Session> localCookieStorage;

    UserTable(SQLDBConnection dbConn) throws SQLException {
        this.dbConn = dbConn;
        dbConn.connect();
        localCookieStorage = new ConcurrentHashMap<>();
    }

    synchronized public int put(User user) {
        if (user == null) {
            return -2;
        }
        //Construct SQL statements.
        String name = user.getName();
        String pass = user.getSerializedSecurePassword();
        String email = user.getEmail();
        String city = user.getCity();
        String description = user.getDescription();
        Session s = user.getCookieObject();
        if (s != null)
            localCookieStorage.put(email, s);
        if (containsUser(email)) {
            return 1; //failed
        }
        String fbLogin = user.getFacebookLoginToken();
        String sqlStat = String.format("INSERT INTO users VALUES (\"%s\", \"%s\", \"%s\", \"%s\", 0, \"%s\", \"%s\");",
                name, email, pass, fbLogin, city, description);
        int check_val;
        try {
            check_val = dbConn.update(sqlStat);
        } catch (SQLException e) {
            e.printStackTrace();
            return 1; //Failed
        }
        return check_val == 1 ? 0 : 1; //On success
    }

    synchronized public int removeUserByEmail(String email) {
        if (email == null) {
            return -2;
        }
        StringBuilder sqlStatement = new StringBuilder();
        sqlStatement.append("DELETE FROM users WHERE email=\"");
        sqlStatement.append(email);
        sqlStatement.append("\";");
        int check_val;
        try {
            check_val = dbConn.update(sqlStatement.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            return 1; //Failed
        }
        return check_val == 1 ? 0 : 1;
    }

    public User findUserByEmail(String email) {
        if (email == null) {
            return null;
        }
        String sqlStat = String.format("SELECT * FROM users WHERE email=\"%s\";", email);
        Statement stmt = null;
        ResultSet rset = null;
        User retval = null;
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStat);
            if (rset.next()) {
                retval = new User();
                retval.setName(rset.getString("name"));
                retval.setEmail(rset.getString("email"));
                retval.setSecurePassword(rset.getString("password"));
                retval.setFacebookLoginToken(rset.getString("fb_token"));
                retval.setCity(rset.getString("city"));
                retval.setDescription(rset.getString("description"));
                Session s = localCookieStorage.get(retval.getEmail());
                retval.setCookie(s);
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public ArrayList<User> findUserByName(String name) {
        if (name == null) {
            return null;
        }
        String sqlStat = String.format("SELECT * FROM users WHERE name LIKE \"%s\";", name);
        Statement stmt = null;
        ResultSet rset = null;
        ArrayList<User> retval = new ArrayList<>();
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStat);
            while (rset.next()) {
                User toadd = new User();
                toadd.setName(rset.getString("name"));
                toadd.setEmail(rset.getString("email"));
                toadd.setSecurePassword(rset.getString("password"));
                toadd.setFacebookLoginToken(rset.getString("fb_token"));
                toadd.setCity(rset.getString("city"));
                toadd.setDescription(rset.getString("description"));
                Session s = localCookieStorage.get(toadd.getEmail());
                toadd.setCookie(s);
                retval.add(toadd);
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public boolean containsUser(String email) {
        if (email == null) {
            return false;
        }
        StringBuilder sqlStatement = new StringBuilder();
        sqlStatement.append("SELECT 1 FROM users WHERE email=\"");
        sqlStatement.append(email);
        sqlStatement.append("\";");
        Statement stmt = null;
        ResultSet rset = null;
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStatement.toString());
            if (rset.next()) {
                rset.close();
                return true;
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;
    }

    synchronized public int changeNameOfUser(String email, String newName) {
        String sqlStmt = 
            String.format("UPDATE users SET name=\"%s\" WHERE email=\"%s\";", newName, email);
        int check_val;
        try {
            check_val = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
            return 1; //Failed
        }
        return check_val == 1 ? 0 : 1;
    }

    public int updateUser(String email, User user) {
        String name = user.getName();
        String city = user.getCity();
        String description = user.getDescription();
        String sqlStmt =
                String.format("UPDATE users SET name=\"%s\", city=\"%s\", description=\"%s\" WHERE email=\"%s\";", name, city, description, email);
        int check_val;
        try {
            check_val = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
            return 1; //Failed
        }
        return check_val == 1 ? 0 : 1;
    }

    synchronized public int changePasswordOfUser(String email, String newpass) {
        String sqlStmt = 
            String.format("UPDATE users SET password=\"%s\" WHERE email=\"%s\";", newpass, email);
        int check_val;
        try {
            check_val = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
            return 1; //Failed
        }
        return check_val == 1 ? 0 : 1;
    }

    public int userLogout(String userEmail) {
        return putUserSession(userEmail, null);
    }

    synchronized public int putUserSession(String userEmail, Session cookie) {
        if (userEmail == null) {
            return -2;
        }
        if (cookie == null) {
            if (!localCookieStorage.containsKey(userEmail)) {
                return 1;
            }
            localCookieStorage.remove(userEmail);
            return 0;
        }
        localCookieStorage.put(userEmail, cookie);
        return 0;
    }

}

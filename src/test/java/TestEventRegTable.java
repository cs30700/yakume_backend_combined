import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by yuxuan on 10/9/16.
 */
public class TestEventRegTable extends TestCase
{
    SQLDBConnection dbConn;
    EventRegistration er;
    @Override
    protected void setUp() throws Exception {
        dbConn = SQLiteConnection.createConnection("yakume.db");
        er = new EventRegistration(dbConn);
    }

    @Test
    public void testAddReg() {
        er.put("a", 1);
        er.put("b", 2);
        assert er.contains("a", 1);
        assert er.contains("a", 2) == false;
        assert er.contains("b", 2);
    }


    @Test
    public void testRemoveReg() {
        er.delete("a", 1);
        er.delete("b", 2);
        assert er.contains("a", 1) == false;
        assert er.contains("b", 2) == false;
    }
}

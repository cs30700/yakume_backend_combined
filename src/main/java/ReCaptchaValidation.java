import com.owlike.genson.Genson;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by yuxuan on 12/2/16.
 */
public class ReCaptchaValidation
{
    static String url = "https://www.google.com/recaptcha/api/siteverify";
    static String secret = Config.config.recaptcha_secret;
    public static boolean validate(String gCaptchaResponse) {
        if (gCaptchaResponse == null) {
            return false;
        }
        Map<String, String> p = new TreeMap<>();
        p.put("secret", secret);
        p.put("response", gCaptchaResponse);
        String response = HTTPRequest.requestSSL(url, HTTPMethod.POST, p);
        Genson g = new Genson();
        ReCaptchaResponse val = g.deserialize(response, ReCaptchaResponse.class);
        return val.success;
    }

    public static void main(String[] args) {
        System.out.println(validate("hello"));
    }
}

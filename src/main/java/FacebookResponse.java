import java.util.HashMap;

/**
 * Created by yuxuan on 10/11/16.
 */

/*
    {
        "data": {
            "app_id": 138483919580948,
            "application": "Social Cafe",
            "expires_at": 1352419328,
            "is_valid": true,
            "issued_at": 1347235328,
            "metadata": {
                "sso": "iphone-safari"
            },
            "scopes": [
                "email",
                "publish_actions"
            ],
            "user_id": 1207059
        }
    }

 */
public class FacebookResponse {
    DataField data;

}

class DataField {
    long app_id;
    String application;
    long expires_at;
    boolean is_valid;
    String[] scopes;
    String user_id;
}
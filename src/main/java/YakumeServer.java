import org.kopitubruk.util.json.JsonObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by yuxuan on 9/13/16.
 */
public class YakumeServer
{
    public static boolean hasInstance;
    public static YakumeServer instance;
    public UserTable userTable;
    public EventTable eventTable;
    public TagEventTable tagTable;
    public AvatarOwnershipTable avatarOwnershipTable;
    public ImageOwnershipTable imageOwnershipTable;
    public EventWatchListTable eventWatchListTable;
    public FollowTable followTable;
    public TimelineTable timelineTable;
    public PaymentTokensTable paymentTokensTable;
    public UserFollowedTags followedTags;
    public EventRegistration transactions;
    public RatingsTable ratingsTable;
    public SQLDBConnection dbConnection;
    public static Log logger;
    static {
        hasInstance = false;
        instance = null;
        logger = new Log(Config.config.log_file);
    }

    private YakumeServer() throws Exception {
        dbConnection = SQLiteConnection.createConnection(Config.config.db_file);
        userTable = new UserTable(dbConnection);
        eventTable = new EventTable(dbConnection);
        tagTable = new TagEventTable(dbConnection);
        transactions = new EventRegistration(dbConnection);
        avatarOwnershipTable = new AvatarOwnershipTable(dbConnection);
        imageOwnershipTable = new ImageOwnershipTable(dbConnection);
        eventWatchListTable = new EventWatchListTable(dbConnection);
        followTable = new FollowTable(dbConnection);
        timelineTable = new TimelineTable(dbConnection);
        followedTags = new UserFollowedTags(dbConnection);
        paymentTokensTable = new PaymentTokensTable(dbConnection);
        ratingsTable = new RatingsTable(dbConnection);
        hasInstance = true;
    }

    public static YakumeServer getInstance() {
        if (!hasInstance) {
            try {
                instance = new YakumeServer();
            } catch (Exception e) {
                logger.error("Database file not found.");
                e.printStackTrace();
                return null;
            }
        }
        return instance;
    }

    public String userRegistration(String username, String email, String pass) {
        try {
            if (userTable.containsUser(email)) {
                logger.print("%s has requested to sign up but email has already taken", email);
                return StatusCode.ERR_USER_EMAIL_TAKEN;
            }

            User newUser = User.createUser(username, email, pass.toCharArray());
            userTable.put(newUser);
            logger.print("%s has successfully signed up.", email);
            EmailSenderManager.sendEmail(email,
                    "Welcome to Project Yakumé",
                    String.format("Dear %s,<br><br>Thank you for signing up at Project Yakum&eacute;.<br><br>" +
                            "If this registration is not initiated by you, please reply immediately.<br><br>" +
                            "Thank you!<br><br>Project Yakum&eacute; Dev Team", username));
            return StatusCode.SUCCESS;
        } catch (Exception e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
        }
        return StatusCode.ERR_EXCEPTION_THROWN;
    }

    public String userRegistrationWithreCaptcha(String username, String email, String pass, String gRecaptcha) {
        try {
            if (userTable.containsUser(email)) {
                logger.print("%s has requested to sign up but email has already taken", email);
                return StatusCode.ERR_USER_EMAIL_TAKEN;
            }

            if (!ReCaptchaValidation.validate(gRecaptcha)) {
                return StatusCode.ERR_RECAPTCHA_INVALID;
            }

            User newUser = User.createUser(username, email, pass.toCharArray());
            userTable.put(newUser);
            logger.print("%s has successfully signed up.", email);
            EmailSenderManager.sendEmail(email,
                    "Welcome to Project Yakumé",
                    String.format("Dear %s,<br><br>Thank you for signing up at Project Yakum&eacute;.<br><br>" +
                            "If this registration is not initiated by you, please reply immediately.<br><br>" +
                            "Thank you!<br><br>Project Yakum&eacute; Dev Team", username));
            return StatusCode.SUCCESS;
        } catch (Exception e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
        }
        return StatusCode.ERR_EXCEPTION_THROWN;
    }

    public String userLogin(String email, String pass) {
        if (!userTable.containsUser(email)) {
            logger.print("%s requested login but email not found", email);
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        User thisUser = userTable.findUserByEmail(email);
        if (!thisUser.login(pass.toCharArray())) {
            logger.print("%s requested login but password is incorrect", email);
            return StatusCode.ERR_PASSWORD_INCORRECT;
        }
        userTable.putUserSession(thisUser.getEmail(), thisUser.getCookieObject());
        logger.print("%s successfully logged in", email);
        return thisUser.getCookie();
    }

    public String greetings(String email, String cookie) {
        if (!userTable.containsUser(email)) {
            logger.print("%s requested hello but email was not found.", email);
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        User thisUser = userTable.findUserByEmail(email);
        if (thisUser.verifySession(cookie)) {
            return String.format("Hello, %s", thisUser.getName());
        }
        logger.print("%s requested hello but not logged in.", email);
        return StatusCode.ERR_NOT_LOGGED_IN;
    }

    public String logout(String email, String cookie) {
        if (email == null || cookie == null) {
            return StatusCode.ERR_INVALID_ARGUMENT;
        }
        userTable.userLogout(email);
        userTable.putUserSession(email, null);
        logger.print("%s has successfully logged out.", email);
        return StatusCode.SUCCESS;
    }

    public String getUserProfile(String email) {
        if (email == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        User theUser = userTable.findUserByEmail(email);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        return theUser.toString();
    }

    public String getEventDetails(int eventID) {
        Event event = eventTable.findEventById(eventID);
        if (event == null) {
            return StatusCode.ERR_EVENT_ID_NOT_FOUND;
        }
        while (event.getTimeOfEvent() < System.currentTimeMillis() && event.getRecurring() != 0) {
            event.setTimeOfEvent(event.getTimeOfEvent() + event.getRecurring());
        }
        eventTable.modifyEvent(eventID, event);
        ArrayList<String> tags = tagTable.getTagsByEventID(eventID);
        event.setTags(tags);
        return event.toString();
    }

    public String createEvent(String name,
                           String description,
                           long time,
                           int duration,
                           ArrayList<String> tags,
                           String address,
                           int zipcode,
                           double longitude,
                           double latitude,
                           String imageFileName,
                           int price,
                           int recurring,
                           String ownerEmail,
                           String sessionID)
    {
        User thisUser = userTable.findUserByEmail(ownerEmail);
        if (thisUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!thisUser.verifySession(sessionID)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        Event toadd = new Event(time, new Location(zipcode, address, longitude, latitude), name, description, tags, duration, ownerEmail);
        toadd.setPrice(price);
        toadd.setRecurring(recurring);
        int evId = eventTable.putEvent(toadd);
        tagTable.addTagsForEvent(evId, tags);
        if (imageFileName != null) {
            this.uploadImageToEvent(ownerEmail, sessionID, evId, imageFileName);
        }
        TimelineEntry tle = new TimelineEntry();
        tle.event = evId;
        tle.userEmail = ownerEmail;
        tle.action = TimelineEntry.ACTION_CREATED;
        tle.time = System.currentTimeMillis();
        timelineTable.addEntry(tle);
        return String.format("%d", evId);
    }

    public String modifyEvent(int id,
                           String title,
                           String description,
                           long time,
                           int duration,
                           ArrayList<String> tags,
                           int zipcode,
                           String address,
                           double longitude,
                           double latitude,
                           int price,
                           int recurring,
                           String ownerEmail,
                           String sessionID
                           ) {
        //TODO: get event from table, update information and store
        User theUser = userTable.findUserByEmail(ownerEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionID)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        Event theEvent = eventTable.findEventById(id);
        if (theEvent == null) {
            return StatusCode.ERR_EVENT_ID_NOT_FOUND;
        }
        if (!theEvent.getOwnerEmail().equals(ownerEmail)) {
            return StatusCode.ERR_PERMISSION_DENIED;
        }
        Event toAdd = new Event(time, new Location(zipcode, address, longitude, latitude), title, description, null, duration, ownerEmail);
        toAdd.setRecurring(recurring);
        toAdd.setPrice(price);
        eventTable.modifyEvent(id, toAdd);
        tagTable.deleteEventFromAll(id);
        tagTable.addTagsForEvent(id, tags);
        return StatusCode.SUCCESS;
    }

    public String modifyUserName(String email, String sessionID, String newname) {
        //TODO: Update name of user
        User theUser = userTable.findUserByEmail(email);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionID)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        userTable.changeNameOfUser(email, newname);
        return StatusCode.SUCCESS;
    }

    public String updateUserProfile(String email, String sessionID, String newName, String newCity, String newDescription) {
        User theUser = userTable.findUserByEmail(email);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionID)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        if (newName != null) {
            theUser.setName(newName);
        }
        if (newCity != null) {
            theUser.setCity(newCity);
        }
        if (newDescription != null) {
            theUser.setDescription(newDescription);
        }
        userTable.updateUser(email, theUser);
        return StatusCode.SUCCESS;
    }

    public String changeUserPassword(String email, String sessionID, String newPassword) {
        User theUser = userTable.findUserByEmail(email);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionID)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        userTable.changePasswordOfUser(email, Password.createPassword(newPassword.toCharArray()).serialize());
        return StatusCode.SUCCESS;
    }

    public String resetUserPassword(String email) {
        User theUser = userTable.findUserByEmail(email);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        String username = theUser.getName();
        String new_rand_password = UUID.randomUUID().toString();
        userTable.changePasswordOfUser(email,
                Password.createPassword(new_rand_password.toCharArray()).serialize());
        EmailSenderManager.sendEmail(email, "Your new password", String.format("Dear %s,<br><br>You just requested to reset your password<br>" +
                "Your temporary password is: %s<br><br>Please change it immediately.<br><br>Project Yakum&eacute; Dev Team"
                , username, new_rand_password));
        return StatusCode.SUCCESS;
    }

    public String registerToEvent(String email, String cookie, int eventID, String paymentToken) {
        if (eventTable.findEventById(eventID) == null) {
            return StatusCode.ERR_EVENT_ID_NOT_FOUND;
        }
        User theUser = userTable.findUserByEmail(email);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(cookie)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        if (transactions.contains(email, eventID)) {
            return StatusCode.ERR_ALREADY_REGISTERED;
        }
        Event theEvent = eventTable.findEventById(eventID);
        if (theEvent.getPrice() != 0) {
            if (paymentTokensTable.validateToken(paymentToken, theEvent.getPrice()) != 0) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
        }
        transactions.put(email, eventID);
        TimelineEntry tle = new TimelineEntry();
        tle.event = eventID;
        tle.userEmail = email;
        tle.action = TimelineEntry.ACTION_RSVPED;
        tle.time = System.currentTimeMillis();
        timelineTable.addEntry(tle);
        return StatusCode.SUCCESS;
    }

    public String unregisterToEvent(String email, String cookie, int eventID) {
        if (eventTable.findEventById(eventID) == null) {
            return StatusCode.ERR_EVENT_ID_NOT_FOUND;
        }
        User theUser = userTable.findUserByEmail(email);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(cookie)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        if (!transactions.contains(email, eventID)) {
            return StatusCode.ERR_NOT_YET_REGISTERED;
        }
        transactions.delete(email, eventID);
        return StatusCode.SUCCESS;
    }

    public String getAllEvents() {
        ArrayList<Event> events = eventTable.getAllEvents();
        for (int i = 0; i < events.size(); i++) {
            Event e = events.get(i);
            int id = e.getId();
            ArrayList<String> tags = tagTable.getTagsByEventID(id);
            e.setTags(tags);
        }
        JsonObject json = new JsonObject();
        ArrayList<JsonObject> obj = new ArrayList<>();
        for (int i = 0; i < events.size(); i++) {
            obj.add(events.get(i).toJsonObject());
        }
        return json.add("events", obj).toJSON();
    }

    public String getAttendees(int eventID) {
        ArrayList<String> ar = transactions.getUsersByEventID(eventID);
        return new JsonObject().add("attendees", ar).toJSON();
    }

    public String getMyEvents(String userEmail, String cookie) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(cookie)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        ArrayList<Integer> ar = transactions.getEventIDsByUserEmail(userEmail);
        return new JsonObject().add("events", ar).toJSON();
    }

    public String deleteEvent(int id, String userEmail, String cookie) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(cookie)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        Event ev = eventTable.findEventById(id);
        if (ev == null) {
            return StatusCode.ERR_EVENT_ID_NOT_FOUND;
        }
        String eventOwner = ev.getOwnerEmail();
        if (eventOwner != null && !eventOwner.equals(userEmail)) {
            return StatusCode.ERR_PERMISSION_DENIED;
        }
        eventTable.deleteEventById(id);
        transactions.deleteAllEntriesByEvent(id);
        tagTable.deleteEventFromAll(id);
        eventWatchListTable.deleteAllEntriesByEvent(id);
        timelineTable.deleteEvent(id);
        return StatusCode.SUCCESS;
    }

    public String hasRegistered(String userEmail, String sessionID, int eventid) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionID)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        return transactions.contains(userEmail, eventid) ? "TRUE" : "FALSE";
    }

    public String loginByFacebook(String userToken, String[] userEmail) {
        if (userToken == null) {
            return StatusCode.ERR_INVALID_ARGUMENT;
        }
        FacebookLogin fblogin = new FacebookLogin(userToken);
        FacebookUserProfile fup = fblogin.getUserProfileIfValid();
        if (fup == null) {
            return StatusCode.ERR_PERMISSION_DENIED;
        }
        String email = fup.email;
        if (email == null) {
            return StatusCode.ERR_FACEBOOK_EMAIL_NOT_SET;
        }
        userEmail[0] = email;
        if (!userTable.containsUser(email)) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        Session session = new Session();
        userTable.putUserSession(email, session);
        return session.toString();
    }

    public String setAvatar(String userEmail, String sessionID, String avatarFileName) {
        if (!userTable.containsUser(userEmail)) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        User theUser = userTable.findUserByEmail(userEmail);
        if (!theUser.verifySession(sessionID)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        avatarOwnershipTable.updateAvatar(userEmail, avatarFileName);
        return StatusCode.SUCCESS;
    }

    public String getAvatarOfUser(String userEmail) {
        String retval = avatarOwnershipTable.getAvatarFilename(userEmail);
        return retval != null ? retval : "default-avatar.png";
    }

    public String uploadImageToEvent(String userEmail, String sessionID, int eventid, String imageFileName) {
        Event theEvent = eventTable.findEventById(eventid);
        if (theEvent == null) {
            return StatusCode.ERR_EVENT_ID_NOT_FOUND;
        }
        if (!theEvent.getOwnerEmail().equals(userEmail)) {
            return StatusCode.ERR_PERMISSION_DENIED;
        }
        User theUser = userTable.findUserByEmail(theEvent.getOwnerEmail());
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionID)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        imageOwnershipTable.addImageToEvent(eventid, imageFileName);
        return StatusCode.SUCCESS;
    }

    public String deleteImageFromEvent(String userEmail, String sessionID, int eventid, String imageFileName) {
        Event theEvent = eventTable.findEventById(eventid);
        if (theEvent == null) {
            return StatusCode.ERR_EVENT_ID_NOT_FOUND;
        }
        if (!theEvent.getOwnerEmail().equals(userEmail)) {
            return StatusCode.ERR_PERMISSION_DENIED;
        }
        User theUser = userTable.findUserByEmail(theEvent.getOwnerEmail());
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionID)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        imageOwnershipTable.removeImageFromEvent(eventid, imageFileName);
        return StatusCode.SUCCESS;
    }

    public String getImageFilenames(int eventId) {
        ArrayList<String> ar = imageOwnershipTable.getImageFilenames(eventId);
        return new JsonObject().add("images", ar).toJSON();
    }

    public String saveEventToWatchlist(String userEmail, String sessionID, int eventId) {
        if (eventTable.findEventById(eventId) == null) {
            return StatusCode.ERR_EVENT_ID_NOT_FOUND;
        }
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionID)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        if (eventWatchListTable.contains(userEmail, eventId)) {
            return StatusCode.ERR_ALREADY_REGISTERED;
        }
        eventWatchListTable.put(userEmail, eventId);
        return StatusCode.SUCCESS;
    }

    public String deleteEventFromWatchList(String email, String cookie, int eventID) {
        if (eventTable.findEventById(eventID) == null) {
            return StatusCode.ERR_EVENT_ID_NOT_FOUND;
        }
        User theUser = userTable.findUserByEmail(email);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(cookie)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        if (!eventWatchListTable.contains(email, eventID)) {
            return StatusCode.ERR_NOT_YET_REGISTERED;
        }
        eventWatchListTable.delete(email, eventID);
        return StatusCode.SUCCESS;
    }

    public String getMyWatchList(String userEmail, String cookie) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(cookie)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        ArrayList<Integer> ar = eventWatchListTable.getEventIDsByUserEmail(userEmail);
        return new JsonObject().add("events", ar).toJSON();
    }

    public String hasWatched(String userEmail, String sessionID, int eventid) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionID)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        return eventWatchListTable.contains(userEmail, eventid) ? "TRUE" : "FALSE";
    }

    public String followUser(String userEmail, String sessionId, String followee) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionId)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        if (!userTable.containsUser(followee)) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        followTable.follow(userEmail, followee);
        return StatusCode.SUCCESS;
    }

    public String unfollowUser(String userEmail, String sessionId, String followee) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionId)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        if (!userTable.containsUser(followee)) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        followTable.unfollow(userEmail, followee);
        return StatusCode.SUCCESS;
    }

    public String getFolloweeList(String userEmail) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        ArrayList<String> followees = followTable.getFollowingList(userEmail);
        return new JsonObject().add("followee", followees).toJSON();
    }

    public String getFollowerList(String userEmail) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        ArrayList<String> followees = followTable.getFollowerList(userEmail);
        return new JsonObject().add("follower", followees).toJSON();
    }

    public String getNumOfFollowees(String userEmail) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        return Integer.toString(followTable.getNumberOfFollowees(userEmail));
    }

    public String getNumOfFollowers(String userEmail) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        return Integer.toString(followTable.getNumOfFollowers(userEmail));
    }

    public String searchEvent(String keyword, long timeStart, long timeEnd, int zipcode, String tag)  {
        try {
            ArrayList<Integer> ar1 = eventTable.filterEvent(zipcode, timeStart, timeEnd);
            ArrayList<Integer> ar2 = tagTable.getEventsByTag(tag);
            Set<Integer> toFind;
            if (tag == null) {
                toFind = new HashSet<>();
                toFind.addAll(ar1);
            } else {
                toFind = Y.intersection(ar1, ar2);
            }
            if (keyword == null) {
                return new JsonObject().add("events", toFind).toJSON();
            }
            Soundex_EventHolder seh = new Soundex_EventHolder(5, 1000);
            for (int i : toFind) {
                seh.write(i, eventTable.findEventById(i).getTitle());
            }
            ArrayList<Integer> ar = new ArrayList<>();

            int[] result = seh.read(keyword);
            for (int i = 0; i < result.length; i++) {
                if (toFind.contains(result[i])) {
                    ar.add(result[i]);
                }
            }
            return new JsonObject().add("events", ar).toJSON();
        } catch (Exception e) {
            e.printStackTrace();
            return StatusCode.ERR_EXCEPTION_THROWN;
        }
    }

    public String payMoney(String cardNumber, String expirationMonth, String expirationYear, int amount) {
        String token = PaymentHandler.authorizeCreditCard(amount, cardNumber, expirationMonth, expirationYear);
        return token;
    }

    public String getTimeline(String user) {
        ArrayList<TimelineEntry> entries = timelineTable.getTimeLineEntries(0, System.currentTimeMillis(), user);
        ArrayList<JsonObject> ar = new ArrayList<>();
        for (int i = 0; i < entries.size(); i++) {
            ar.add(entries.get(i).toJsonObject());
        }
        return new JsonObject().add("timeline", ar).toJSON();
    }

    public String getNewsfeed(String userEmail, String sessionId) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionId)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        ArrayList<String> followees = followTable.getFollowingList(userEmail);
        ArrayList<JsonObject> result = new ArrayList<>();
        for (int i = 0; i < followees.size(); i++) {
            ArrayList<TimelineEntry> ar = timelineTable.getTimeLineEntries(0, System.currentTimeMillis(), followees.get(i));
            for (int j = 0; j < ar.size(); j++) {
                result.add(ar.get(j).toJsonObject());
            }
        }
        return new JsonObject().add("newsfeed", result).toJSON();
    }

    public String eventsCreated(String email) {
        ArrayList<Event> events = eventTable.searchEventByOwner(email);
        for (int i = 0; i < events.size(); i++) {
            Event e = events.get(i);
            int id = e.getId();
            ArrayList<String> tags = tagTable.getTagsByEventID(id);
            e.setTags(tags);
        }
        JsonObject json = new JsonObject();
        ArrayList<JsonObject> obj = new ArrayList<>();
        for (int i = 0; i < events.size(); i++) {
            obj.add(events.get(i).toJsonObject());
        }
        return json.add("events", obj).toJSON();
    }

    public String searchEventByTag(String tag) {
        ArrayList<Integer> ar = tagTable.getEventsByTag(tag);
        ArrayList<JsonObject> events = new ArrayList<>();
        for (int i = 0; i < ar.size(); i++) {
            Event ev = eventTable.findEventById(ar.get(i));
            ev.setTags(tagTable.getTagsByEventID(ar.get(i)));
            events.add(ev.toJsonObject());
        }
        return new JsonObject().add("events", events).toJSON();
    }

    public String followTag(String userEmail, String sessionId, String tag) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionId)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        followedTags.follow(userEmail, tag);
        return StatusCode.SUCCESS;
    }

    public String unfollowTag(String userEmail, String sessionId, String tag) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionId)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        followedTags.unfollow(userEmail, tag);
        return StatusCode.SUCCESS;
    }

    public String getFollowedTags(String userEmail) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        ArrayList<String> tags = followedTags.getFollowingTags(userEmail);
        return new JsonObject().add("tags", tags).toJSON();
    }

    public String getTagNews(String userEmail) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        ArrayList<String> tags = followedTags.getFollowingTags(userEmail);
        Set<Integer> events = new HashSet<>();
        for (String tag : tags) {
            ArrayList<Integer> ar = tagTable.getEventsByTag(tag);
            events.addAll(ar);
        }
        return new JsonObject().add("events", events.toArray()).toJSON();
    }

    public String searchUserByName(String userName) {
        if (userName == null) {
            return null;
        }
        ArrayList<User> ar = userTable.findUserByName(userName);
        ArrayList<JsonObject> json = new ArrayList<>();
        for (User u : ar) {
            json.add(u.toJsonObject());
        }
        return new JsonObject().add("users", json).toJSON();
    }

    public String getRatings(String userEmail) {
        if (userEmail == null) {
            return StatusCode.ERR_INVALID_ARGUMENT;
        }
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        return Double.toString(ratingsTable.getRating(userEmail));
    }

    public String updateRating(String userEmail, String sessionId, String ratee, int score) {
        User theUser = userTable.findUserByEmail(userEmail);
        if (theUser == null) {
            return StatusCode.ERR_USER_NOT_FOUND;
        }
        if (!theUser.verifySession(sessionId)) {
            return StatusCode.ERR_NOT_LOGGED_IN;
        }
        if (score > 5 || score < 1) {
            return StatusCode.ERR_INVALID_ARGUMENT;
        }
        ratingsTable.updateRating(userEmail, ratee, score);
        return StatusCode.SUCCESS;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        dbConnection.close();
    }
}

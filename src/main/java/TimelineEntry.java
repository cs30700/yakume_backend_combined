import org.kopitubruk.util.json.JsonObject;

public class TimelineEntry
{
    public static final String ACTION_FOLLOWED = "followed";
    public static final String ACTION_RSVPED = "RSVPed to";
    public static final String ACTION_CREATED = "created";
    public long time;
    public String userEmail;
    public String action;
    public int event;

    @Override
    public String toString() {
        return new JsonObject()
                .add("time", time)
                .add("email", userEmail)
                .add("action", action)
                .add("event", event)
                .toJSON();
    }

    public JsonObject toJsonObject() {
        return new JsonObject()
                .add("time", time)
                .add("email", userEmail)
                .add("action", action)
                .add("event", event);
    }
}

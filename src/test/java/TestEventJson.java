import java.util.ArrayList;
import com.owlike.genson.*;

/**
 * Created by yuxuan on 10/8/16.
 */
public class TestEventJson
{
    public static void main(String[] args) {
        ArrayList<String> ar = new ArrayList<>();
        ar.add("A");
        ar.add("B");
        Event myEv = new Event(System.currentTimeMillis() + 12345678, new Location(47906, null, 1.1, 2.2), "Hello", "World", ar, 100, "hello@wor.ld");
        System.out.println(myEv.toJsonObject());
        Genson genson = new Genson();
        String json = genson.serialize(myEv);
        System.out.println(genson.serialize(myEv));
    }
}

import java.util.ArrayList;

/**
 * Created by yuxuan on 10/11/16.
 */
public class TestTagTable
{
    public static void main(String[] args) throws Exception {
        ArrayList<String> tags = new ArrayList<>();
        tags.add("A");
        tags.add("B");
        TagEventTable t = new TagEventTable(SQLiteConnection.createConnection("yakume.db"));
        t.addTagsForEvent(1, tags);
    }
}

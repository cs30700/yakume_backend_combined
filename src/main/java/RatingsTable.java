/**
 * Created by yuxuan on 11/24/16.
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * DB Schema
 * CREATE TABLE rating(
 *      user VARCHAR(512),
 *      rater VARCHAR(512),
 *      rate INT
 * );
 */
public class RatingsTable {
    SQLDBConnection dbConn;

    public RatingsTable(SQLDBConnection dbConn) throws SQLException {
        this.dbConn = dbConn;
        dbConn.connect();
    }

    public int updateRating(String rater, String user, int score) {
        removeRating(rater, user);
        String prepareStatement = String.format("INSERT INTO rating VALUES(\"%s\", \"%s\", %d);", user, rater, score);
        try {
            dbConn.update(prepareStatement);
        } catch (SQLException e) {
            e.printStackTrace();
            return 2;
        }
        return 0;
    }

    public int removeRating(String rater, String user) {
        String prepareStatement = String.format("DELETE FROM rating WHERE rater=\"%s\" AND user=\"%s\";", rater, user);
        try {
            dbConn.update(prepareStatement);
        } catch (SQLException e) {
            e.printStackTrace();
            return 2;
        }
        return 0;
    }

    public double getRating(String user) {
        String prepareStmt = String.format("SELECT rate FROM rating WHERE user=\"%s\";", user);
        Statement stmt = null;
        ResultSet rset = null;
        int sum = 0;
        int rate = 0;
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(prepareStmt);
            while (rset.next()) {
                sum += rset.getInt("rate");
                rate++;
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        if (rate == 0) {
            return 0;
        }
        return ((double)sum) / rate;
    }
}

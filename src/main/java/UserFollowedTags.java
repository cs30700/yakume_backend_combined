/**
 * Created by yuxuan on 11/6/16.
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * DB Schema
 * CREATE TABLE followed_tags(
        follower VARCHAR(256),
        tag VARCHAR(256)
   );
 */
public class UserFollowedTags {
    SQLDBConnection dbConn;

    public UserFollowedTags(SQLDBConnection dbConn) throws SQLException {
        this.dbConn = dbConn;
        this.dbConn.connect();
    }

    public int follow(String followerEmail, String tag) {
        if (hasFollowed(followerEmail, tag)) {
            return 0;
        }
        String sqlStmt = String.format("INSERT INTO followed_tags VALUES(\"%s\", \"%s\");", followerEmail, tag);
        int check = 0;
        try {
            check = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check == 1 ? 0 : 1;
    }

    public int unfollow(String followerEmail, String tag) {
        String sqlStmt = String.format("DELETE FROM followed_tags WHERE follower=\"%s\" AND tag=\"%s\";", followerEmail, tag);
        int check = 0;
        try {
            check = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check == 1 ? 0 : 1;
    }

    public boolean hasFollowed(String followerEmail, String tag) {
        String sqlStmt = String.format("SELECT 1 FROM followed_tags WHERE follower=\"%s\" AND tag=\"%s\";", followerEmail, tag);
        int check = 0;
        Statement stmt = null;
        ResultSet rset;
        boolean retval = false;
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            if (rset.next()) {
                retval = true;
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public ArrayList<String> getFollowingTags(String userEmail) {
        String sqlStmt = String.format("SELECT * FROM followed_tags WHERE follower=\"%s\";", userEmail);
        int check = 0;
        Statement stmt = null;
        ResultSet rset;
        ArrayList<String> retval = new ArrayList<>();
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            while (rset.next()) {
                retval.add(rset.getString("tag"));
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public ArrayList<String> getFollowerList(String tag) {
        String sqlStmt = String.format("SELECT * FROM followed_tags WHERE tag=\"%s\";", tag);
        int check = 0;
        Statement stmt = null;
        ResultSet rset;
        ArrayList<String> retval = new ArrayList<>();
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            while (rset.next()) {
                retval.add(rset.getString("follower"));
            }
            rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public int getNumOfFollowers(String userEmail) {
        return getFollowerList(userEmail).size();
    }

    public int getNumberOfFollowingTags(String userEmail) {
        return getFollowingTags(userEmail).size();
    }
}

import com.owlike.genson.Genson;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by yuxuan on 10/24/16.
 */
public class Config
{
    public static Config config;
    static {
        try {
            config = readConfig("server.conf");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int port;
    public String db_file;
    public int max_thread;
    public String email_addr;
    public String email_pass;
    public int time_out;
    public String log_file;
    public int cookie_validity_sec;
    public String facebook_app_id;
    public String facebook_app_secret;
    public String recaptcha_secret;

    public static Config readConfig(String fileName) throws FileNotFoundException {
        File fd = new File(fileName);
        Scanner fr = new Scanner(fd);
        StringBuilder builder = new StringBuilder();
        while (fr.hasNext()) {
            builder.append(fr.nextLine());
        }
        Genson myGenson = new Genson();
        return myGenson.deserialize(builder.toString(), Config.class);
    }

}

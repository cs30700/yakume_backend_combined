/**
 * Created by yuxuan on 8/26/16.
 */
public class SQLiteDatabaseConnectionErrorException extends Exception {
    public SQLiteDatabaseConnectionErrorException(String message) {
        super(message);
    }
}

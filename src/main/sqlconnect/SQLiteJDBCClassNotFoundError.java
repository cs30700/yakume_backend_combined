/**
 * Created by yuxuan on 8/26/16.
 */
public class SQLiteJDBCClassNotFoundError extends Error {
    public SQLiteJDBCClassNotFoundError(String message) {
        super(message);
    }
}

import spark.ExceptionHandler;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

import static spark.Spark.*;

/**
 * Created by yuxuan on 5/9/16.
 */

public class WebController
{
    private final static int VALIDITY = Config.config.cookie_validity_sec;
    public static void main(String[] args) {
        port(Config.config.port);
        threadPool(Config.config.max_thread);

        YakumeServer serverInstance = YakumeServer.getInstance();
        assert serverInstance != null; //Something is really wrong.

        post("/login", (request, response) -> {
            String email = request.queryParams("email");
            String password = request.queryParams("password");
            if (email == null || password == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            System.out.println("Login requested by " + email);
            String res = serverInstance.userLogin(email, password);
            if (res.startsWith("ERR")) {
                return res;
            }
            response.cookie("email", email, VALIDITY);
            response.cookie("session", res, VALIDITY);
            return StatusCode.SUCCESS;
        });
        post("/register", ((request, response) -> {
            String name = request.queryParams("name");
            String pass = request.queryParams("password");
            String email = request.queryParams("email");
            if (name == null || pass == null || email == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }

            System.out.println("Register requested by " + email);
            return serverInstance.userRegistration(name, email, pass);
        }));

        post("/registerwithrecaptcha", ((request, response) -> {
            String name = request.queryParams("name");
            String pass = request.queryParams("password");
            String email = request.queryParams("email");
            String reCaptcha = request.queryParams("recaptcha");
            if (name == null || pass == null || email == null || reCaptcha == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }

            System.out.println("Register requested by " + email);
            return serverInstance.userRegistrationWithreCaptcha(name, email, pass, reCaptcha);
        }));

        get("/hello", ((request, response) -> {
            String email = request.cookie("email");
            String cookie = request.cookie("session");
            if (email == null || cookie == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            System.out.println("Hello requested by " + email);
            return serverInstance.greetings(email, cookie);
        }));

        get("/logout", ((request, response) -> {
            String email = request.cookie("email");
            String cookie = request.cookie("session");
            if (email == null || cookie == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            return serverInstance.logout(email, cookie);
        }));

        post("/addevent", ((request, response) -> {
            String email = request.cookie("email");
            String sessionID = request.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String title = request.queryParams("title");
            String description = request.queryParams("description");
            String address = request.queryParams("address");
            String tagsStr = request.queryParams("tags");
            String price = request.queryParams("price");
            String recur = request.queryParams("recurring");

            if (price == null) {
                price = "0";
            }
            if (recur == null) {
                recur = "0";
            }
            if (title == null || description == null || address == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            ArrayList<String> tags = new ArrayList<String>();
            if (tagsStr != null) {
                String[] tagsArr = tagsStr.split(" ");
                for (int i = 0; i < tagsArr.length; i++) {
                    tags.add(tagsArr[i]);
                }
            }
            double longitude, latitude;
            long time;
            int zipcode;
            int duration;
            int price_i;
            int recur_i;

            try {
                longitude = Double.valueOf(request.queryParams("longitude"));
                latitude = Double.valueOf(request.queryParams("latitude"));
                time = Long.valueOf(request.queryParams("timestamp"));
                duration = Integer.valueOf(request.queryParams("duration"));
                zipcode = Integer.valueOf(request.queryParams("zipcode"));
                price_i = Integer.valueOf(price);
                recur_i = Integer.valueOf(recur);
            } catch (Exception e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            String imageFileName = request.queryParams("filename");
            return serverInstance.createEvent(title, description, time, duration, tags, address,
                    zipcode, longitude, latitude, imageFileName, price_i, recur_i, email, sessionID);
        }));

        get("/getevent", ((request, response) -> {
            String id = request.queryParams("eventid");
            if (id == null) {
                return serverInstance.getAllEvents();
            }
            int eventID;
            try {
                eventID = Integer.valueOf(id);
            } catch (NumberFormatException e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            return serverInstance.getEventDetails(eventID);
        }));

        get("/userprofile", ((request, response) -> {
            String email = request.queryParams("email");
            if (email == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.getUserProfile(email);
        }));

        post("/userprofile/update", (req, res) -> {
            String email = req.cookie("email");
            String sessionID = req.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String name = req.queryParams("name");
            String city = req.queryParams("city");
            String description = req.queryParams("description");
            return serverInstance.updateUserProfile(email, sessionID, name, city, description);
        });

        get("/searchuser", (req, res) -> {
            String name = req.queryParams("name");
            if (name == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.searchUserByName(name);
        });

        post("/pay", (req, res) -> {
            String cardNum = req.queryParams("card");
            String expMonth = req.queryParams("exp_mm");
            String expYear = req.queryParams("exp_yy");
            String amount = req.queryParams("amount");
            int amount_i;
            try {
                amount_i = Integer.valueOf(amount);
            } catch (Exception e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            return serverInstance.payMoney(cardNum, expMonth, expYear, amount_i);
        });

        post("/event/register", ((request, response) -> {
            String email = request.cookie("email");
            String sessionID = request.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String eventID = request.queryParams("eventid");
            String paymentToken = request.queryParams("payment_token");
            if (eventID == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            int id;
            try {
                id = Integer.valueOf(eventID);
            } catch (NumberFormatException e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            return serverInstance.registerToEvent(email, sessionID, id, paymentToken);
        }));

        post("/event/unregister", ((request, response) -> {
            String email = request.cookie("email");
            String sessionID = request.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String eventID = request.queryParams("eventid");
            if (eventID == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            int id;
            try {
                id = Integer.valueOf(eventID);
            } catch (NumberFormatException e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            return serverInstance.unregisterToEvent(email, sessionID, id);
        }));

        get("/myevents", ((request, response) -> {
            String userEmail = request.cookie("email");
            String sessionID = request.cookie("session");
            if (userEmail == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            return serverInstance.getMyEvents(userEmail, sessionID);
        }));

        //TODO: created events
        get("/eventcreated", (request, response) -> {
            String userEmail = request.cookie("email");
            if (request.queryParams("email") != null) {
                userEmail = request.queryParams("email");
            }
            if (userEmail == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.eventsCreated(userEmail);
        });

        get("/hasreged", ((request, response) -> {
            String userEmail = request.cookie("email");
            String sessionID = request.cookie("session");
            if (userEmail == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String evId = request.queryParams("eventid");
            if (evId == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            int id;
            try {
                id = Integer.valueOf(evId);
            } catch (NumberFormatException e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            return serverInstance.hasRegistered(userEmail, sessionID, id);
        }));

        get("/attendees", ((request, response) -> {
            String evId = request.queryParams("eventid");
            if (evId == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            int id;
            try {
                id = Integer.valueOf(evId);
            } catch (NumberFormatException e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            return serverInstance.getAttendees(id);
        }));

        post("/resetpassword", ((request, response) -> {
            String useremail = request.queryParams("email");
            if (useremail == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.resetUserPassword(useremail);
        }));

        post("/changepassword", (request, response) -> {
            String userEmail = request.cookie("email");
            String sessionID = request.cookie("session");
            if (userEmail == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String password = request.queryParams("password");
            if (password == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.changeUserPassword(userEmail, sessionID, password);
        });

        post("/deleteevent", (request, response) -> {
            String userEmail = request.cookie("email");
            String sessionID = request.cookie("session");
            if (userEmail == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String evId = request.queryParams("eventid");
            if (evId == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            int id;
            try {
                id = Integer.valueOf(evId);
            } catch (NumberFormatException e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            return serverInstance.deleteEvent(id, userEmail, sessionID);
        });

        post("/changeusername", ((request, response) -> {
            String userEmail = request.cookie("email");
            String sessionID = request.cookie("session");
            if (userEmail == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String newname = request.queryParams("newname");
            if (newname == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.modifyUserName(userEmail, sessionID, newname);
        }));

        post("/modifyevent", (request, response) -> {
            String userEmail = request.cookie("email");
            String sessionID = request.cookie("session");
            if (userEmail == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String evId = request.queryParams("eventid");
            String title = request.queryParams("title");
            String description = request.queryParams("description");
            String address = request.queryParams("address");
            String tagsStr = request.queryParams("tags");
            String price = request.queryParams("price");
            String recur = request.queryParams("recurring");
            if (price == null) {
                price = "0";
            }
            ArrayList<String> tags = new ArrayList<>();
            if (tagsStr != null) {
                String[] tagsArr = tagsStr.split(" ");
                for (int i = 0; i < tagsArr.length; i++) {
                    tags.add(tagsArr[i]);
                }
            }
            if (evId == null || title == null || description == null || address == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            int id, price_i, recurring;
            double longitude, latitude;
            long time;
            int zipcode, duration;
            try {
                id = Integer.valueOf(evId);
                longitude = Double.valueOf(request.queryParams("longitude"));
                latitude = Double.valueOf(request.queryParams("latitude"));
                time = Long.valueOf(request.queryParams("timestamp"));
                duration = Integer.valueOf(request.queryParams("duration"));
                zipcode = Integer.valueOf(request.queryParams("zipcode"));
                price_i = Integer.valueOf(price);
                if (recur != null) {
                    recurring = Integer.valueOf(recur);
                } else {
                    recurring = 0;
                }
            } catch (Exception e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            return serverInstance.modifyEvent(id, title, description, time, duration, tags,
                    zipcode, address, longitude, latitude, price_i, recurring, userEmail, sessionID);
        });

        post("/fblogin", ((request, response) -> {
            String fbUserToken = request.queryParams("fbtoken");
            if (fbUserToken == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            String[] emailArr = new String[1];
            String res = serverInstance.loginByFacebook(fbUserToken, emailArr);
            if (res.startsWith("ERR")) {
                return res;
            }
            response.cookie("email", emailArr[0], VALIDITY);
            response.cookie("session", res, VALIDITY);
            return StatusCode.SUCCESS;
        }));

        get("/avatar/get", ((request, response) -> {
            String userEmail = request.queryParams("email");
            if (userEmail == null) {
                userEmail = request.cookie("email");
            }
            if (userEmail == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.getAvatarOfUser(userEmail);
        }));

        post("/avatar/update", (request, response) -> {
            String userEmail = request.cookie("email");
            String sessionID = request.cookie("session");
            if (userEmail == null || sessionID == null) {
                return StatusCode.ERR_NOT_LOGGED_IN;
            }
            String avatarFileName = request.queryParams("filename");
            if (avatarFileName == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.setAvatar(userEmail, sessionID, avatarFileName);
        });

        post("/event/image/upload", (request, response) -> {
            String userEmail = request.cookie("email");
            String sessionID = request.cookie("session");
            String evId = request.queryParams("eventid");
            if (userEmail == null || sessionID == null || evId == null) {
                return StatusCode.ERR_NOT_LOGGED_IN;
            }
            String imageFileName = request.queryParams("filename");

            int eventId = 0;
            try {
                eventId = Integer.valueOf(evId);
            } catch (NumberFormatException e) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            if (imageFileName == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.uploadImageToEvent(userEmail, sessionID, eventId, imageFileName);
        });

        post("/event/image/delete", (request, response) -> {
            String userEmail = request.cookie("email");
            String sessionID = request.cookie("session");
            String evId = request.queryParams("eventid");
            if (userEmail == null || sessionID == null || evId == null) {
                return StatusCode.ERR_NOT_LOGGED_IN;
            }
            String imageFileName = request.queryParams("filename");

            int eventId = 0;
            try {
                eventId = Integer.valueOf(evId);
            } catch (NumberFormatException e) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            if (imageFileName == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.deleteImageFromEvent(userEmail, sessionID, eventId, imageFileName);
        });

        get("/event/image/get", ((request, response) -> {
            String evId = request.queryParams("eventid");
            if (evId == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            int eventId = 0;
            try {
                eventId = Integer.valueOf(evId);
            } catch (NumberFormatException e) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.getImageFilenames(eventId);
        }));

        post("/watchlist/save", (request, response) -> {
            String email = request.cookie("email");
            String sessionID = request.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String eventID = request.queryParams("eventid");
            if (eventID == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            int id;
            try {
                id = Integer.valueOf(eventID);
            } catch (NumberFormatException e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            return serverInstance.saveEventToWatchlist(email, sessionID, id);
        });

        post("/watchlist/delete", (request, response) -> {
            String email = request.cookie("email");
            String sessionID = request.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String eventID = request.queryParams("eventid");
            if (eventID == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            int id;
            try {
                id = Integer.valueOf(eventID);
            } catch (NumberFormatException e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            return serverInstance.deleteEventFromWatchList(email, sessionID, id);
        });

        get("/watchlist", ((request, response) -> {
            String userEmail = request.cookie("email");
            String sessionID = request.cookie("session");
            if (userEmail == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            return serverInstance.getMyWatchList(userEmail, sessionID);
        }));

        get("/watchlist/haswatched", (req, res) -> {
            String userEmail = req.cookie("email");
            String sessionID = req.cookie("session");
            String eventId = req.queryParams("eventid");
            if (userEmail == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            if (eventId == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            int evId = 0;
            try {
                evId = Integer.valueOf(eventId);
            } catch (Exception e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            return serverInstance.hasWatched(userEmail, sessionID, evId);
        });

        get("/event/search", ((req, res) -> {
            String keyword = req.queryParams("keyword");
            String filterLocation = req.queryParams("zipcode");
            String filterTimeStart = req.queryParams("time_start");
            String filterTimeEnd = req.queryParams("time_end");
            String filterTag = req.queryParams("tag");
            int zipcode;
            long timeStart, timeEnd;
            if (filterLocation != null) {
                try
                {
                    zipcode = Integer.valueOf(filterLocation);
                } catch (Exception e) {
                    return StatusCode.ERR_NUM_FORMAT;
                }
            } else {
                zipcode = 0;
            }
            if (filterTimeStart != null) {
                try
                {
                    timeStart = Integer.valueOf(filterTimeStart);
                } catch (Exception e) {
                    return StatusCode.ERR_NUM_FORMAT;
                }
            } else {
                timeStart = -1;
            }
            if (filterTimeEnd != null) {
                try
                {
                    timeEnd = Integer.valueOf(filterTimeEnd);
                } catch (Exception e) {
                    return StatusCode.ERR_NUM_FORMAT;
                }
            } else {
                timeEnd = -1;
            }
            return serverInstance.searchEvent(keyword, timeStart, timeEnd, zipcode, filterTag);
        }));

        get("/system/status", ((request, response) -> {
            System.out.println("status requested.");
            return "System is operational";
        }));

        get("/timeline/my", (req, res) -> {
            String email = req.cookie("email");
            if (email == null) {
                return StatusCode.ERR_NOT_LOGGED_IN;
            }
            return serverInstance.getTimeline(email);
        });

        get("/timeline/user", (req, res) -> {
            String email = req.queryParams("email");
            if (email == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.getTimeline(email);
        });

        get("/user/followers/num", (req, res) -> {
            String email = req.cookie("email");
            if (req.queryParams("email") != null) {
                email = req.queryParams("email");
            }
            return serverInstance.getNumOfFollowers(email);
        });

        get("/user/followees/num", (req, res) -> {
            String email = req.cookie("email");
            if (req.queryParams("email") != null) {
                email = req.queryParams("email");
            }
            return serverInstance.getNumOfFollowees(email);
        });

        get("/user/followers", (req, res) -> {
            String email = req.cookie("email");
            if (req.queryParams("email") != null) {
                email = req.queryParams("email");
            }
            return serverInstance.getFollowerList(email);
        });

        get("/user/followees", (req, res) -> {
            String email = req.cookie("email");
            if (req.queryParams("email") != null) {
                email = req.queryParams("email");
            }
            if (email == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.getFolloweeList(email);
        });

        get("/newsfeed", (req, res) -> {
            String email = req.cookie("email");
            String sessionID = req.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            return serverInstance.getNewsfeed(email, sessionID);
        });

        get("/tags/news", (req, res) -> {
            String email = req.cookie("email");
            String sessionID = req.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            return serverInstance.getTagNews(email);
        });

        get("/tagevents", (req, res) -> {
            String tag = req.queryParams("tag");
            if (tag == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.searchEventByTag(tag);
        });

        post("/rating/rate", (req, res) -> {
            String email = req.cookie("email");
            String sessionID = req.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String user = req.queryParams("user");
            String rating = req.queryParams("rating");
            int rating_i;
            try
            {
                rating_i = Integer.valueOf(rating);
            } catch (Exception e) {
                return StatusCode.ERR_NUM_FORMAT;
            }
            return serverInstance.updateRating(email, sessionID, user, rating_i);
        });

        get("/rating/get", (req, res) -> {
            String email = req.cookie("email");
            String user = req.queryParams("user");
            return serverInstance.getRatings(user == null ? email : user);
        });

        post("/tag/follow", (req, res) -> {
            String email = req.cookie("email");
            String sessionID = req.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String tag = req.queryParams("tag");
            if (tag == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.followTag(email, sessionID, tag);
        });

        post("/tag/unfollow", (req, res) -> {
            String email = req.cookie("email");
            String sessionID = req.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String tag = req.queryParams("tag");
            if (tag == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.unfollowTag(email, sessionID, tag);
        });

        post("/user/follow", (req, res) -> {
            String email = req.cookie("email");
            String sessionID = req.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String email1 = req.queryParams("email");
            if (email1 == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.followUser(email, sessionID, email1);
        });

        post("/user/unfollow", (req, res) -> {
            String email = req.cookie("email");
            String sessionID = req.cookie("session");
            if (email == null || sessionID == null) {
                return StatusCode.ERR_INVALID_COOKIES;
            }
            String email1 = req.queryParams("email");
            if (email1 == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.unfollowUser(email, sessionID, email1);
        });

        get("/tag/followed", (req, res) -> {
            String email = req.cookie("email");
            if (req.queryParams("email") != null) {
                email = req.queryParams("email");
            }
            if (email == null) {
                return StatusCode.ERR_INVALID_ARGUMENT;
            }
            return serverInstance.getFollowedTags(email);
        });

        options("/*", (((request, response) -> "ALLOW")));
        System.out.println("Initialization is complete.");
    }
}

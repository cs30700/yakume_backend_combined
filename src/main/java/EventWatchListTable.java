import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * EventRegistration Class
 * Interact with a database table
 */

/**
 * DB Schema
 CREATE TABLE saved_events(
     id INTEGER PRIMARY KEY AUTOINCREMENT,
     eventid INTEGER,
     useremail VARCHAR(256)
 );
 */
public class EventWatchListTable
{
    SQLDBConnection dbConn;

    public EventWatchListTable(SQLDBConnection dbConn) throws SQLException {
        this.dbConn = dbConn;
        dbConn.connect();
    }

    public ArrayList<String> getUsersByEventID(int eventID) {
        ArrayList<String> retval = new ArrayList<>();
        String sqlStmt = String.format("SELECT * FROM saved_events WHERE eventid=%d;", eventID);
        Statement stmt = null;
        ResultSet rset = null;
        try {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            while (rset.next()) {
                retval.add(rset.getString("useremail"));
            }
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return retval;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    YakumeServer.logger.print(e.toString());
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public ArrayList<Integer> getEventIDsByUserEmail(String userEmail) {
        if (userEmail == null) {
            return new ArrayList<>();
        }
        ArrayList<Integer> retval = new ArrayList<>();
        String sqlStmt = String.format("SELECT * FROM saved_events WHERE useremail=\"%s\";", userEmail);
        Statement stmt = null;
        ResultSet rset = null;
        try {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            while (rset.next()) {
                retval.add(rset.getInt("eventid"));
            }
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return retval;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    YakumeServer.logger.print(e.toString());
                    e.printStackTrace();
                }
            }
        }
        return retval;
    }

    public int put(String userEmail, int eventid) {
        if (userEmail == null) {
            return -3;
        }
        if (contains(userEmail, eventid)) {
            return -1;
        }
        String sqlStmt = String.format("INSERT INTO saved_events(useremail, eventid) VALUES (\"%s\", %d);", userEmail, eventid);
        int check;
        try {
            check = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return -1;
        }
        if (check != 1) {
            return -2;
        }
        return lastInsertId();
    }

    public int delete(String userEmail, int eventid) {
        if (userEmail == null) {
            return -2;
        }
        if (!contains(userEmail, eventid)) {
            return 1;
        }
        String sqlStmt = String.format("DELETE FROM saved_events WHERE useremail=\"%s\" AND eventid=%d;", userEmail, eventid);
        int check;
        try {
            check = dbConn.update(sqlStmt);
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return -1;
        }
        return check == 1 ? 0 : 2;
    }

    public boolean contains(String userEmail, int eventid) {
        if (userEmail == null) {
            return false;
        }
        String sqlStmt = String.format("SELECT 1 FROM saved_events WHERE useremail=\"%s\" AND eventid=%d;", userEmail, eventid);
        Statement stmt = null;
        ResultSet rset = null;
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(sqlStmt);
            if (rset.next()) {
                return true;
            }
            rset.close();
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return false;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    YakumeServer.logger.print(e.toString());
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public int deleteAllEntriesByEvent(int id) {
        String sqlStmt = String.format("DELETE FROM saved_events WHERE eventid=%d;", id);
        try {
            dbConn.update(sqlStmt);
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    public int lastInsertId() {
        String lookupLast = "SELECT last_insert_rowid() AS id FROM saved_events;";
        Statement stmt = null;
        ResultSet rset = null;
        try
        {
            stmt = dbConn.getStatement();
            rset = stmt.executeQuery(lookupLast);
            if (rset.next()) {
                return rset.getInt("id");
            }
        } catch (SQLException e) {
            YakumeServer.logger.print(e.toString());
            e.printStackTrace();
            return -1;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    YakumeServer.logger.print(e.toString());
                    e.printStackTrace();
                }
            }
        }
        return -1;
    }
}

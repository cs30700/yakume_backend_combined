import com.owlike.genson.Genson;
import org.kopitubruk.util.json.JsonObject;

/**
 * Created by yuxuan on 10/11/16.
 */
public class FacebookLogin
{
    static final String app_id = Config.config.facebook_app_id;
    String token;
    static final String APP_SECRET = Config.config.facebook_app_secret;

    FacebookLogin(String user_token) {
        this.token = user_token;
    }

    public String getAccessToken() {
        String url = String.format(
                "https://graph.facebook.com/oauth/access_token" +
                "?client_id=%s" +
                "&client_secret=%s" +
                "&grant_type=client_credentials"
                , app_id, APP_SECRET);
        String response = HTTPRequest.request(url, HTTPMethod.GET);
        return response.substring(13);
    }

    public FacebookUserProfile getUserProfileIfValid() {
        try {
            String accessToken = getAccessToken();
            String url = String.format("https://graph.facebook.com/debug_token?input_token=%s&access_token=%s", token, accessToken);
            String response = HTTPRequest.request(url, HTTPMethod.GET);
            Genson genson = new Genson();
            FacebookResponse fr = genson.deserialize(response, FacebookResponse.class);
            String uid = fr.data.user_id;
            if (!fr.data.is_valid) {
                return null;
            }
            if (fr.data.expires_at * 1000 < System.currentTimeMillis()) {
                return null;
            }
            url = String.format("https://graph.facebook.com/v2.8/%s?access_token=%s&fields=id,name,email", uid, accessToken);
            response = HTTPRequest.request(url, HTTPMethod.GET);
            FacebookUserProfile fup = genson.deserialize(response, FacebookUserProfile.class);
            return fup;
        } catch (Exception e) {
            return null;
        }
    }
/*
    public static void main(String[] args) {
        System.out.println(new FacebookLogin(null).getAccessToken());
    }
*/
}

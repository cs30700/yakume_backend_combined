/**
 * Created by yuxuan on 8/27/16.
 */

public class SQLiteConnectTest {
    public static void main(String[] args) throws Exception {
        SQLiteConnection myconn = SQLiteConnection.createConnection("test.db");
        myconn.connect();
        myconn.update("CREATE TABLE mytable(a PRIMARY KEY, b)");
        myconn.update("DROP TABLE mytable;");
        myconn.close();
    }
}

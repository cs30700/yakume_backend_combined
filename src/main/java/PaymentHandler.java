import com.sun.org.apache.xerces.internal.impl.xpath.regex.RegularExpression;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by yuxuan on 11/6/16.
 */
public class PaymentHandler
{
    public static String authorizeCreditCard(int amountInCents, String cardNumber, String expMonth, String expYear) {
        if (cardNumber == null || expMonth == null || expYear == null) {
            return "FAIL";
        }
        Map<String, String> map = new TreeMap<>();
        map.put("x-pay-token", "#FxAq$foHz/vfv{A0#6twu1}v5Am9Ln7{Epawqei");
        map.put("amount", String.format("%.2f", amountInCents / 100.0));
        map.put("cardNumber", cardNumber);
        map.put("cardExpirationMonth", expMonth);
        map.put("cardExpirationYear", expYear);

        try {
            HTTPRequest.request("https://sandbox.api.visa.com/cybersource/payments/v1/authorizations?apikey=2PSFJ2RAGKOMR165BJTH21iDBU3xon1zcNqSmJjmwwQ6SnT4U",
                    HTTPMethod.POST, map);
        } catch (Exception e) {
            return "FAIL";
        }
        return YakumeServer.instance.paymentTokensTable.createToken(amountInCents);
    }
}

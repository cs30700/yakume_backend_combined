import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashSet;
import java.util.Set;

/**
 * Shortcuts class
 */
public class Y
{
    public static void printf(String format, Object... args) {
        String s = new Formatter().format(format, args).toString();
        System.out.print(s);
    }

    //copied from an editorial http://www.programcreek.com/2015/05/leetcode-intersection-of-two-arrays-java/
    public static Set<Integer> intersection(ArrayList<Integer> ar1, ArrayList<Integer> ar2) {
        HashSet<Integer> set1 = new HashSet<>();
        for (int i : ar1){
            set1.add(i);
        }

        HashSet<Integer> set2 = new HashSet<>();
        for (int i : ar2){
            if(set1.contains(i)){
                set2.add(i);
            }
        }

        return set2;
    }
}

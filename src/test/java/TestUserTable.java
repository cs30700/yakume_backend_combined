/**
 * Created by yuxuan on 10/3/16.
 */
import com.sun.deploy.config.UnixPlatform;
import junit.framework.*;
import org.junit.*;
import org.junit.Test;

public class TestUserTable extends TestCase
{
    UserTable ut;

    @Override
    public void setUp() throws Exception {
        ut = new UserTable(SQLiteConnection.createConnection("yakume.db"));
    }

    @Test
    synchronized public void testAdd() {
        ut.removeUserByEmail("world");
        User u = User.createUser("hello world", "world", "hello_world".toCharArray());
        int check = ut.put(u);
        assertEquals(0, check);
        assertTrue(ut.containsUser("world"));
        User u2 = ut.findUserByEmail("world");
        assert u2.getName().equals("hello world");
        check = ut.removeUserByEmail("world");
        assertEquals(0, check);
        assertFalse(ut.containsUser("world"));

    }

    @Test
    synchronized public void testCookie() {
        Session s = new Session();
        User u = User.createUser("hello", "world", "hello_world".toCharArray());
        String str = s.getSessionCookieIntegrityCheckCode();
        u.setCookie(s);
        ut.put(u);
        User new_u = ut.findUserByEmail("world");
        assert new_u.getCookieObject().isValid();
        assert new_u.getCookieObject().isValid(str);
        int check = ut.removeUserByEmail("world");
        assertEquals(0, check);
        assertFalse(ut.containsUser("world"));
    }

}

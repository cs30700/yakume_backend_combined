import org.kopitubruk.util.json.JsonObject;
import java.util.ArrayList;

/**
 * Created by yuxuan on 9/17/16.
 */


public class Event {
    int id;
    long timePosted;
    long timeOfEvent;
    int secondsDuration;
    Location location;
    String title;
    String description;
    ArrayList<String> tags;
    String ownerEmail;
    int price; //in cents
    ArrayList<String> images;
    int recurring;

    public int getRecurring() {
        return recurring;
    }

    public void setRecurring(int recurring) {
        this.recurring = recurring;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public void setTimePosted(long timePosted) {
        this.timePosted = timePosted;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public void setImagesFromDatabase() {
        images = YakumeServer.instance.imageOwnershipTable.getImageFilenames(id);
    }

    Event() {};

    public Event(long timeOfEvent, Location location, String title, String description, ArrayList<String> tags, int secondsDuration, String ownerEmail) {
        timePosted = System.currentTimeMillis();
        this.timeOfEvent = timeOfEvent;
        this.location = location;
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.secondsDuration = secondsDuration;
        this.ownerEmail = ownerEmail;
    }

    public long getTimePosted() {
        return timePosted;
    }

    public long getTimeOfEvent() {
        return timeOfEvent;
    }

    public void setTimeOfEvent(long timeOfEvent) {
        this.timeOfEvent = timeOfEvent;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public int getSecondsDuration() {
        return secondsDuration;
    }

    public void setSecondsDuration(int secondsDuration) {
        this.secondsDuration = secondsDuration;
    }

    public JsonObject toJsonObject() {
        setImagesFromDatabase();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("id", id)
                .add("title", title)
                .add("description", description)
                .add("zipcode", location.zipcode)
                .add("address", location.address)
                .add("longitude", location.longitude)
                .add("latitude", location.latitude)
                .add("timeposted", timePosted)
                .add("time", timeOfEvent)
                .add("duration", secondsDuration)
                .add("tags", tags)
                .add("owner", ownerEmail)
                .add("price", price)
                .add("images", images)
                .add("recurring", recurring);
        return jsonObject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (id != event.id) return false;
        if (timePosted != event.timePosted) return false;
        if (timeOfEvent != event.timeOfEvent) return false;
        if (secondsDuration != event.secondsDuration) return false;
        if (price != event.price) return false;
        if (location != null ? !location.equals(event.location) : event.location != null) return false;
        if (title != null ? !title.equals(event.title) : event.title != null) return false;
        if (description != null ? !description.equals(event.description) : event.description != null) return false;
        if (tags != null ? !tags.equals(event.tags) : event.tags != null) return false;
        return ownerEmail != null ? ownerEmail.equals(event.ownerEmail) : event.ownerEmail == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (int) (timePosted ^ (timePosted >>> 32));
        result = 31 * result + (int) (timeOfEvent ^ (timeOfEvent >>> 32));
        result = 31 * result + secondsDuration;
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (ownerEmail != null ? ownerEmail.hashCode() : 0);
        result = 31 * result + price;
        return result;
    }

    @Override
    public String toString() {
        return toJsonObject().toJSON();
    }
}

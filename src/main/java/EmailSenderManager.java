/**
 * Created by yuxuan on 10/9/16.
 */
public class EmailSenderManager
{
    public static void sendEmail(String toAddress, String subject, String body) {
        EmailSender es = new EmailSender(toAddress, subject, body);
        new Thread(es).start();
    }
}
